﻿using OverhaulMe.Infrastructure.Commands.Interfaces;

namespace OverhaulMe.Infrastructure.Commands.Users
{
    public class ChangeUserPassword : AuthenticatedCommandBase
    {
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
    }
}