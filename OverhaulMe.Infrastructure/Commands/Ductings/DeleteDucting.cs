﻿using OverhaulMe.Infrastructure.Commands.Interfaces;
using System;

namespace OverhaulMe.Infrastructure.Commands.Ductings
{
    public class DeleteDucting : AuthenticatedCommandBase
    {
        public Guid RoomId { get; set; }
        public string Type { get; set; }
    }
}