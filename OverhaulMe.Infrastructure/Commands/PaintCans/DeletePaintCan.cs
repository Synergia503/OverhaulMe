﻿using System;

namespace OverhaulMe.Infrastructure.Commands.PaintCans
{
    public class DeletePaintCan : AuthenticatedCommandBase
    {
        public Guid RoomId { get; set; }
        public int CanNumbersToRemove { get; set; }
        public string BrandName { get; set; }
    }
}