﻿using OverhaulMe.Infrastructure.Commands.Interfaces;
using System;

namespace OverhaulMe.Infrastructure.Commands.PaintCans
{
    public class CreatePaintCan : AuthenticatedCommandBase
    {
        public Guid RoomId { get; set; }
        public string Colour { get; set; }
        public string BrandName { get; set; }
        public string Place { get; set; }
        public float BoughtVolume { get; set; }
        public float RemainingVolume { get; set; }
        public decimal Price { get; set; }
        public DateTime PurchasedAt { get; set; }
        public DateTime? PaintedAt { get; set; }
    }
}