﻿using OverhaulMe.Infrastructure.Commands.Interfaces;
using System;

namespace OverhaulMe.Infrastructure.Commands.Devices
{
    public class DeleteDevice : AuthenticatedCommandBase
    {
        public Guid RoomId { get; set; }
        public Guid DeviceId { get; set; }
    }
}