﻿using OverhaulMe.Infrastructure.Commands.Interfaces;
using System;


namespace OverhaulMe.Infrastructure.Commands.Devices
{
    public class CreateDevice : AuthenticatedCommandBase
    {
        public Guid RoomId { get; set; }
        public string DeviceName { get; set; }
        public decimal DevicePrice { get; set; }
        public bool IsNewPurchased { get; set; }
        public DateTime? PurchasedAt { get; set; }
    }
}