﻿using OverhaulMe.Infrastructure.Commands.Interfaces;
using System;

namespace OverhaulMe.Infrastructure.Commands.Rooms
{
    public class DeleteRoom : AuthenticatedCommandBase
    {
        public Guid RoomId { get; set; }
    }
}