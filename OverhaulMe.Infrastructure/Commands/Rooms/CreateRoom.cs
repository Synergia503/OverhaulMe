﻿using System;

namespace OverhaulMe.Infrastructure.Commands.Rooms
{
    public class CreateRoom : AuthenticatedCommandBase
    {        
        public Guid RoomId { get; set; }
        public string RoomName { get; set; }
        public float FloorSize { get; set; }
        public float WallSize { get; set; }
        public float CeilingSize { get; set; }
    }
}