﻿using OverhaulMe.Infrastructure.Commands.Interfaces;
using System;

namespace OverhaulMe.Infrastructure.Commands.Furnitures
{
    public class DeleteFurniture : AuthenticatedCommandBase
    {
        public Guid RoomId { get; set; }
        public Guid FurnitureId { get; set; }
    }
}