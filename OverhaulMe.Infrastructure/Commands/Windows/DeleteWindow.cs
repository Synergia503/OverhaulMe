﻿using System;

namespace OverhaulMe.Infrastructure.Commands.Windows
{
    public class DeleteWindow : AuthenticatedCommandBase
    {
        public Guid RoomId { get; set; }
        public Guid WindowId { get; set; }
    }
}