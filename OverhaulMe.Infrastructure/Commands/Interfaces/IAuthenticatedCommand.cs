﻿using System;

namespace OverhaulMe.Infrastructure.Commands.Interfaces
{
    public interface IAuthenticatedCommand : ICommand
    {
        Guid UserId { get; set; }
    }
}