﻿using OverhaulMe.Infrastructure.Commands.Interfaces;
using System;

namespace OverhaulMe.Infrastructure.Commands.Doors
{
    public class DeleteDoor : AuthenticatedCommandBase
    {
        public Guid RoomId { get; set; }
        public string FirstPlace { get; set; }
        public string SecondPlace { get; set; }
    }
}