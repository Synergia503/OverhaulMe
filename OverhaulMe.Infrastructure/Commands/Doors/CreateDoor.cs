﻿using OverhaulMe.Infrastructure.Commands.Interfaces;
using System;

namespace OverhaulMe.Infrastructure.Commands.Doors
{
    public class CreateDoor : AuthenticatedCommandBase
    {
        public Guid RoomId { get; set; }
        public string SecondPlace { get; set; }
        public float Height { get; set; }
        public float Width { get; set; }
        public float Depth { get; set; }
        public decimal Price { get; set; }
        public string BrandName { get; set; }
        public string Kind { get; set; }
        public bool IsNewPurchased { get; set; }
        public DateTime? PurchasedAt { get; set; }
        public DateTime? AssembledAt { get; set; }
        public DateTime? WorkStartedAt { get; set; }
        public DateTime? WorkEndedAt { get; set; }
        public float WorkHours { get; set; }
    }
}