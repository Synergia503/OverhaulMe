﻿namespace OverhaulMe.Infrastructure.Settings
{
    public class GeneralSettings
    {
        public string Name { get; set; }
        public bool SeedRandomData { get; set; }
        public bool SeedSpecificData { get; set; }
    }
}