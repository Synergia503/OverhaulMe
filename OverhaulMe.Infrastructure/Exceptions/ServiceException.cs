﻿using OverhaulMe.Core.DomainExceptions;
using System;

namespace OverhaulMe.Infrastructure.Exceptions
{
    public class ServiceException : Exception, IDomainException
    {
        protected ServiceException()
        {
        }

        public ServiceException(string errorCode) : this(errorCode, null, null, null)
        {

        }

        public ServiceException(string message, params object[] args) : this(null, null, message, args)
        {

        }

        protected ServiceException(string errorCode, string message, params object[] args)
           : this(errorCode, null, message, args)
        {

        }


        public ServiceException(Exception innerException, string message, params object[] args)
            : this(string.Empty, innerException, message, args)
        {

        }

        public ServiceException(string errorCode, Exception innerException, string message, params object[] args)
            : base(string.Format(message, args), innerException)
        {
            ErrorCode = errorCode;
        }

        public string ErrorCode { get; }
    }
}