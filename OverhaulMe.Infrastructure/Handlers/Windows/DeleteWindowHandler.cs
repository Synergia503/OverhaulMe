﻿using OverhaulMe.Infrastructure.Commands.Interfaces;
using OverhaulMe.Infrastructure.Commands.Windows;
using OverhaulMe.Infrastructure.Services;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Handlers.Windows
{
    public class DeleteWindowHandler : ICommandHandler<DeleteWindow>
    {
        private readonly IWindowProvider _windowProvider;

        public DeleteWindowHandler(IWindowProvider windowProvider)
        {
            _windowProvider = windowProvider;
        }

        public async Task HandleAsync(DeleteWindow command)
        {
            await _windowProvider.RemoveWindowAsync(command.UserId, command.RoomId, command.WindowId);
        }
    }
}