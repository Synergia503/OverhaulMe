﻿using OverhaulMe.Infrastructure.Commands.Interfaces;
using OverhaulMe.Infrastructure.Commands.Windows;
using OverhaulMe.Infrastructure.Services;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Handlers.Windows
{
    public class CreateWindowHandler : ICommandHandler<CreateWindow>
    {
        private readonly IWindowProvider _windowProvider;
        public CreateWindowHandler(IWindowProvider windowProvider)
        {
            _windowProvider = windowProvider;
        }

        public async Task HandleAsync(CreateWindow command)
        {
            await _windowProvider.AddWindowAsync(command.UserId,
                command.RoomId, command.WindowId, command.Place,
                command.Height, command.Width, command.Depth,
                command.Price, command.BrandName, command.Kind,
                command.IsNewPurchased, command.PurchasedAt, command.AssembledAt);
        }
    }
}