﻿using OverhaulMe.Infrastructure.Commands.Interfaces;
using OverhaulMe.Infrastructure.Commands.Rooms;
using OverhaulMe.Infrastructure.Services;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Handlers.Rooms
{
    public class DeleteRoomHandler : ICommandHandler<DeleteRoom>
    {
        private readonly IRoomService _roomService;
        public DeleteRoomHandler(IRoomService roomService)
        {
            _roomService = roomService;
        }

        public async Task HandleAsync(DeleteRoom command)
        {            
            await _roomService.DeleteAsync(command.UserId, command.RoomId);
        }
    }
}