﻿using OverhaulMe.Infrastructure.Commands.Interfaces;
using OverhaulMe.Infrastructure.Commands.Rooms;
using OverhaulMe.Infrastructure.Services;
using System;
using System.Threading.Tasks;

namespace OverhaulMe.Core.Handlers
{
    public class CreateRoomHandler : ICommandHandler<CreateRoom>
    {
        private readonly IRoomService _roomService;
        public CreateRoomHandler(IRoomService roomService)
        {
            _roomService = roomService;
        }

        public async Task HandleAsync(CreateRoom command)
        {
            command.RoomId = Guid.NewGuid();
            await _roomService.CreateRoomAsync(command.UserId, command.RoomId, command.RoomName, command.FloorSize, command.WallSize, command.CeilingSize);
        }
    }
}