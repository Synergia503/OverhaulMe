﻿using OverhaulMe.Infrastructure.Commands.Interfaces;
using OverhaulMe.Infrastructure.Commands.PaintCans;
using OverhaulMe.Infrastructure.Services;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Handlers.PaintCans
{
    public class DeletePaintCanHandler : ICommandHandler<DeletePaintCan>
    {
        private readonly IPaintCanProvider _paintCanProvider;
        public DeletePaintCanHandler(IPaintCanProvider paintCanProvider)
        {
            _paintCanProvider = paintCanProvider;
        }

        public async Task HandleAsync(DeletePaintCan command)
        {
            await _paintCanProvider.RemovePaintCanAsync(command.UserId, command.RoomId, command.CanNumbersToRemove, command.BrandName);
        }
    }
}