﻿using OverhaulMe.Infrastructure.Commands.Interfaces;
using OverhaulMe.Infrastructure.Commands.PaintCans;
using OverhaulMe.Infrastructure.Services;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Handlers.PaintCans
{
    public class CreatePaintCanHandler : ICommandHandler<CreatePaintCan>
    {
        private readonly IPaintCanProvider _paintCanProvider;
        public CreatePaintCanHandler(IPaintCanProvider paintCanProvider)
        {
            _paintCanProvider = paintCanProvider;
        }

        public async Task HandleAsync(CreatePaintCan command)
        {
            await _paintCanProvider.AddPaintCanAsync(command.UserId,
                command.RoomId, command.Colour, command.BrandName, command.BoughtVolume, command.RemainingVolume, command.Price, command.PurchasedAt, command.PaintedAt);
        }
    }
}