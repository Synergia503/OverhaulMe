﻿using OverhaulMe.Infrastructure.Commands.Furnitures;
using OverhaulMe.Infrastructure.Commands.Interfaces;
using OverhaulMe.Infrastructure.Services;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Handlers.Furnitures
{
    public class DeleteFurnitureHandler : ICommandHandler<DeleteFurniture>
    {
        private readonly IFurnitureProvider _furnitureProvider;
        public DeleteFurnitureHandler(IFurnitureProvider furnitureProvider)
        {
            _furnitureProvider = furnitureProvider;
        }

        public async Task HandleAsync(DeleteFurniture command)
        {
            await _furnitureProvider.RemoveFurnitureAsync(command.UserId, command.RoomId, command.FurnitureId);
        }
    }
}