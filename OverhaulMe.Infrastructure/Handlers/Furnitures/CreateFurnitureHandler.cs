﻿using OverhaulMe.Infrastructure.Commands.Furnitures;
using OverhaulMe.Infrastructure.Commands.Interfaces;
using OverhaulMe.Infrastructure.Services;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Handlers.Furnitures
{
    public class CreateFurnitureHandler : ICommandHandler<CreateFurniture>
    {
        private readonly IFurnitureProvider _furnitureProvider;
        public CreateFurnitureHandler(IFurnitureProvider furnitureProvider)
        {
            _furnitureProvider = furnitureProvider;
        }

        public async Task HandleAsync(CreateFurniture command)
        {
            await _furnitureProvider.AddFurnitureAsync(command.UserId,
                command.RoomId, command.FurnitureId, command.Name,
                command.Price, command.Place, command.Purpose, command.BoughtPlace,
                command.IsNewPurchased, command.PurchasedAt, command.MountedAt
                );
        }
    }
}