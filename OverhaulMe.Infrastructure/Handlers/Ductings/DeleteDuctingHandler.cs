﻿using OverhaulMe.Infrastructure.Commands.Ductings;
using OverhaulMe.Infrastructure.Commands.Interfaces;
using OverhaulMe.Infrastructure.Services;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Handlers.Ductings
{
    public class DeleteDuctingHandler : ICommandHandler<DeleteDucting>
    {
        private readonly IDuctingProvider _ductingProvider;

        public DeleteDuctingHandler(IDuctingProvider ductingProvider)
        {
            _ductingProvider = ductingProvider;
        }

        public async Task HandleAsync(DeleteDucting command)
        {
            await _ductingProvider.RemoveDuctingAsync(command.UserId, command.RoomId, command.Type);
        }
    }
}