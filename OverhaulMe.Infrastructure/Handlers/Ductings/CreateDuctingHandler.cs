﻿using OverhaulMe.Infrastructure.Commands.Ductings;
using OverhaulMe.Infrastructure.Commands.Interfaces;
using OverhaulMe.Infrastructure.Services;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Handlers.Ductings
{
    public class CreateDuctingHandler : ICommandHandler<CreateDucting>
    {
        private readonly IDuctingProvider _ductingProvider;
        public CreateDuctingHandler(IDuctingProvider ductingProvider)
        {
            _ductingProvider = ductingProvider;
        }

        public async Task HandleAsync(CreateDucting command)
        {
            await _ductingProvider.AddDuctingAsync(command.UserId, command.RoomId, command.Type, command.Length, command.Price, command.Place, command.BoughtPlace, command.PurchasedAt, command.AssembledAt);
        }
    }
}