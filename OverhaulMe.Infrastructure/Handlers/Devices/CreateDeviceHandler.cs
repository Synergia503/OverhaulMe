﻿using OverhaulMe.Infrastructure.Commands.Devices;
using OverhaulMe.Infrastructure.Commands.Interfaces;
using OverhaulMe.Infrastructure.Services;
using System;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Handlers.Devices
{
    public class CreateDeviceHandler : ICommandHandler<CreateDevice>
    {
        private readonly IRoomDeviceManager _roomDeviceManager;
        public CreateDeviceHandler(IRoomDeviceManager roomDeviceManager)
        {
            _roomDeviceManager = roomDeviceManager;
        }

        public async Task HandleAsync(CreateDevice command)
        {
            await _roomDeviceManager.CreateDeviceAsync(command.UserId, command.RoomId, Guid.NewGuid(), command.DeviceName, command.DevicePrice, command.IsNewPurchased, command.PurchasedAt);
        }
    }
}