﻿using OverhaulMe.Infrastructure.Commands.Devices;
using OverhaulMe.Infrastructure.Commands.Interfaces;
using OverhaulMe.Infrastructure.Services;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Handlers.Devices
{
    public class DeleteDeviceHandler : ICommandHandler<DeleteDevice>
    {
        private readonly IRoomDeviceManager _roomDeviceManager;
        public DeleteDeviceHandler(IRoomDeviceManager roomDeviceManager)
        {
            _roomDeviceManager = roomDeviceManager;
        }
        public async Task HandleAsync(DeleteDevice command)
        {
            await _roomDeviceManager.RemoveDeviceAsync(command.UserId, command.RoomId, command.DeviceId);
        }
    }
}