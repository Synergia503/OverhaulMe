﻿using OverhaulMe.Infrastructure.Commands.Doors;
using OverhaulMe.Infrastructure.Commands.Interfaces;
using OverhaulMe.Infrastructure.Services;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Handlers.Doors
{
    public class CreateDoorHandler : ICommandHandler<CreateDoor>
    {
        private readonly IDoorProvider _doorProvider;
        public CreateDoorHandler(IDoorProvider doorProvider)
        {
            _doorProvider = doorProvider;
        }

        public async Task HandleAsync(CreateDoor command)
        {
            await _doorProvider.AddDoorAsync(command.UserId,
                command.RoomId, command.SecondPlace, command.Height,
                command.Width, command.Depth, command.Price, command.BrandName,
                command.Kind, command.IsNewPurchased, command.WorkHours, command.PurchasedAt,
                command.AssembledAt, command.WorkStartedAt, command.WorkEndedAt
                );
        }
    }
}