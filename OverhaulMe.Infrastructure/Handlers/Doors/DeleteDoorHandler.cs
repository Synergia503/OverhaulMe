﻿using OverhaulMe.Infrastructure.Commands.Doors;
using OverhaulMe.Infrastructure.Commands.Interfaces;
using OverhaulMe.Infrastructure.Services;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Handlers.Doors
{
    class DeleteDoorHandler : ICommandHandler<DeleteDoor>
    {
        private readonly IDoorProvider _doorProvider;
        public DeleteDoorHandler(IDoorProvider doorProvider)
        {
            _doorProvider = doorProvider;
        }

        public async Task HandleAsync(DeleteDoor command)
        {
            await _doorProvider.RemoveDoorAsync(command.UserId, command.RoomId, command.FirstPlace, command.SecondPlace);
        }
    }
}