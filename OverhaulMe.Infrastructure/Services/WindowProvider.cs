﻿using AutoMapper;
using OverhaulMe.Core.Domain;
using OverhaulMe.Core.Repositories;
using OverhaulMe.Infrastructure.DTO;
using OverhaulMe.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Services
{
    public class WindowProvider : IWindowProvider
    {

        private readonly IRoomRepository _roomRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public WindowProvider(IRoomRepository roomRepository, IUserRepository userRepository, IMapper mapper)
        {
            _roomRepository = roomRepository;
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task AddWindowAsync(Guid userId, Guid roomId, Guid windowId, string place, float height, float width, float depth, decimal price, string brandName, string kind, bool isNewPurchased, DateTime purchasedAt, DateTime? assembledAt)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var room = await _roomRepository.GetSingleRoomForUser(user.Id, roomId);
            var window = Window.Create(room, windowId, height, width, depth, price, brandName, kind, isNewPurchased, purchasedAt, assembledAt);
            room.AddWindow(window);
            await _roomRepository.UpdateAsync(room);
        }

        public async Task<IEnumerable<WindowDto>> BrowseAsync(Guid userId)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var rooms = await _roomRepository.GetRoomsForUser(user.Id);
            var windows = rooms.SelectMany(r => r.Windows);
            return _mapper.Map<IEnumerable<WindowDto>>(windows);
        }

        public async Task<IEnumerable<WindowDto>> BrowseForRoomAsync(Guid userId, Guid roomId)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var room = await _roomRepository.GetSingleRoomForUser(user.Id, roomId);
            return _mapper.Map<IEnumerable<WindowDto>>(room.Windows);
        }

        public async Task<WindowDto> GetWindowAsync(Guid userId, Guid windowId)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var rooms = await _roomRepository.GetRoomsForUser(user.Id);
            var window = rooms.SelectMany(w => w.Windows).SingleOrDefault(w => w.Id == windowId);
            return _mapper.Map<WindowDto>(window);
        }

        public async Task RemoveWindowAsync(Guid userId, Guid roomId, Guid windowId)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var room = await _roomRepository.GetSingleRoomForUser(user.Id, roomId);
            var window = room.Windows.SingleOrDefault(w => w.Id == windowId);
            room.RemoveWindow(window);
            await _roomRepository.UpdateAsync(room);
        }

        public async Task UpdateWindowAsync(Guid roomId, Guid windowId)
        {
            throw new NotImplementedException();
        }
    }
}