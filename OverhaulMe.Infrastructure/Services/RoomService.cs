﻿using AutoMapper;
using OverhaulMe.Core.Domain;
using OverhaulMe.Core.DomainExceptions;
using OverhaulMe.Core.Repositories;
using OverhaulMe.Infrastructure.DTO;
using OverhaulMe.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Services
{
    public class RoomService : IRoomService
    {
        private readonly IRoomRepository _roomRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public RoomService(IRoomRepository roomRepository, IUserRepository userRepository, IMapper mapper)
        {
            _roomRepository = roomRepository;
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<RoomDetailsDto> GetAsync(Guid userId, Guid roomId)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var room = await _roomRepository.GetSingleRoomForUser(user.Id, roomId);
            return _mapper.Map<RoomDetailsDto>(room);
        }

        public async Task<IEnumerable<RoomDto>> BrowseAsync(Guid userId)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var rooms = await _roomRepository.GetRoomsForUser(user.Id);
            return _mapper.Map<IEnumerable<RoomDto>>(rooms);
        }

        public async Task CreateRoomAsync(Guid userId, Guid roomId, string roomName, float floorSize, float wallSize, float ceilingSize)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var room = await _roomRepository.GeByIdAsync(roomId);
            if (room != null)
            {
                throw new RoomException(ErrorCodes.RoomAlreadyExists, $"Room with id: '{room.Id}' already exists.");
            }

            room = Room.Create(user.Id, roomId, roomName, floorSize, wallSize, ceilingSize);
            await _roomRepository.AddAsync(room);
        }

        public async Task DeleteAsync(Guid userId, Guid roomId)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var room = await _roomRepository.GetSingleRoomForUser(user.Id, roomId);
            await _roomRepository.DeleteAsync(room);
        }
    }
}