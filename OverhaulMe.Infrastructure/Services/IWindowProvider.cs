﻿using OverhaulMe.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Services
{
    public interface IWindowProvider : IService
    {
        Task AddWindowAsync(Guid userId, Guid roomId, Guid windowId, string place, float height, float width, float depth, decimal price, string brandName, string kind, bool isNewPurchased, DateTime purchasedAt, DateTime? assembledAt);
        Task<WindowDto> GetWindowAsync(Guid userId, Guid windowId);
        Task<IEnumerable<WindowDto>> BrowseAsync(Guid userId);
        Task<IEnumerable<WindowDto>> BrowseForRoomAsync(Guid userId, Guid roomId);
        Task RemoveWindowAsync(Guid userId, Guid roomId, Guid windowId);
        Task UpdateWindowAsync(Guid roomId, Guid windowId);
    }
}