﻿using OverhaulMe.Infrastructure.DTO;
using System;

namespace OverhaulMe.Infrastructure.Services.Jwt
{
    public interface IJwtHandler
    {
        JwtDto CreateToken(Guid userId, string role);
    }
}