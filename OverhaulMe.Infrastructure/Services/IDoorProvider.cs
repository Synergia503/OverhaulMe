﻿using OverhaulMe.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Services
{
    public interface IDoorProvider : IService
    {
        Task<DoorDto> GetSingleDoorAsync(Guid userId, string firstPlace, string secondPlace);
        Task<IEnumerable<DoorDto>> BrowseAsync(Guid userId);
        Task<IEnumerable<DoorDto>> BrowseForRoomAsync(Guid userId, Guid roomId);
        Task AddDoorAsync(Guid userId, Guid roomId, string secondPlace, float height, float width, float depth, decimal price, string brandName, string kind, bool isNewPurchased, float workHours, DateTime? purchasedAt = null, DateTime? assembledAt = null, DateTime? workStartedAt = null, DateTime? workEndedAt = null);
        Task RemoveDoorAsync(Guid userId, Guid roomId, string firstPlace, string secondPlace);
        Task UpdateDoorAsync(Guid roomId, string firstPlace, string secondPlace);
    }
}