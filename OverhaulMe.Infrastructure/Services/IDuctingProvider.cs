﻿using OverhaulMe.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Services
{
    public interface IDuctingProvider : IService
    {
        Task<IEnumerable<DuctingDto>> GetByTypeAsync(Guid userId, string type);
        Task<IEnumerable<DuctingDto>> BrowseForRoomAsync(Guid userId, Guid roomId);
        Task<IEnumerable<DuctingDto>> BrowseAllAsync(Guid userId);
        Task AddDuctingAsync(Guid userId, Guid roomId, string type, float length, decimal price, string place, string boughtPlace, DateTime purchasedAt, DateTime? assembledAt = null);
        Task RemoveDuctingAsync(Guid userId, Guid roomId, string type);
        Task UpdateDuctingAsync(Guid roomId);
    }
}