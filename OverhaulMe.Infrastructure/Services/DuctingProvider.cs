﻿using AutoMapper;
using OverhaulMe.Core.Domain;
using OverhaulMe.Core.DomainExceptions;
using OverhaulMe.Core.Repositories;
using OverhaulMe.Infrastructure.DTO;
using OverhaulMe.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Services
{
    public class DuctingProvider : IDuctingProvider
    {
        private readonly IRoomRepository _roomRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public DuctingProvider(IRoomRepository roomRepository, IUserRepository userRepository, IMapper mapper)
        {
            _roomRepository = roomRepository;
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task AddDuctingAsync(Guid userId, Guid roomId, string type, float length, decimal price, string place, string boughtPlace, DateTime purchasedAt, DateTime? assembledAt = null)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var room = await _roomRepository.GetSingleRoomForUser(user.Id, roomId);
            var ductingBuilder = new DuctingBuilder();
            ductingBuilder.Create(roomId, type)
                 .Cost(price)
                 .Length(length)
                 .PlacesConfiguration()
                 .BoughtIn(boughtPlace)
                 .StayingIn(place)
                 .DatesConfiguration()
                 .PurchaseDate(purchasedAt)
                 .AssemblyDate(assembledAt);

            var ducting = ductingBuilder.Build();
            if (room.Ductings.Any(d => d.Type == ducting.Type))
            {
                throw new DuctingException(ErrorCodes.DuctingAlreadyExists, $"Ducting with type: '{type}' already exists in room with id: '{roomId}'.");
            }

            room.AddDucting(ducting);
            await _roomRepository.UpdateAsync(room);
        }

        public async Task<IEnumerable<DuctingDto>> BrowseAllAsync(Guid userId)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var rooms = await _roomRepository.GetRoomsForUser(user.Id);
            var ductings = rooms.SelectMany(r => r.Ductings);
            return _mapper.Map<IEnumerable<DuctingDto>>(ductings);
        }

        public async Task<IEnumerable<DuctingDto>> BrowseForRoomAsync(Guid userId, Guid roomId)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var room = await _roomRepository.GetSingleRoomForUser(user.Id, roomId);
            var ductingsPerSingleRoom = room.Ductings;
            return _mapper.Map<IEnumerable<DuctingDto>>(ductingsPerSingleRoom);
        }

        public async Task<IEnumerable<DuctingDto>> GetByTypeAsync(Guid userId, string type)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var rooms = await _roomRepository.GetRoomsForUser(user.Id);
            var ductings = rooms.SelectMany(r => r.Ductings).Where(d => d.Type == type);
            return _mapper.Map<IEnumerable<DuctingDto>>(ductings);
        }

        public async Task RemoveDuctingAsync(Guid userId, Guid roomId, string type)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var room = await _roomRepository.GetSingleRoomForUser(user.Id, roomId);
            var ducting = room.Ductings.SingleOrDefault(d => d.Type == type);
            room.RemoveDucting(ducting);
            await _roomRepository.UpdateAsync(room);
        }

        public async Task UpdateDuctingAsync(Guid roomId)
        {
            throw new NotImplementedException();
        }
    }
}