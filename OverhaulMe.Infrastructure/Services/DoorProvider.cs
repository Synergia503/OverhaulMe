﻿using AutoMapper;
using OverhaulMe.Core.Domain;
using OverhaulMe.Core.Repositories;
using OverhaulMe.Infrastructure.DTO;
using OverhaulMe.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Services
{
    public class DoorProvider : IDoorProvider
    {
        private readonly IRoomRepository _roomRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public DoorProvider(IRoomRepository roomRepository, IUserRepository userRepository, IMapper mapper)
        {
            _roomRepository = roomRepository;
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task AddDoorAsync(Guid userId, Guid roomId, string secondPlace, float height, float width, float depth, decimal price, string brandName, string kind, bool isNewPurchased, float workHours, DateTime? purchasedAt = null, DateTime? assembledAt = null, DateTime? workStartedAt = null, DateTime? workEndedAt = null)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var room = await _roomRepository.GetSingleRoomForUser(user.Id, roomId);
            var door = Door.Create(room, secondPlace, height, width, depth, price, brandName, kind, isNewPurchased, workHours, purchasedAt, assembledAt, workStartedAt, workEndedAt);
            room.AddDoor(door);
            await _roomRepository.UpdateAsync(room);
        }

        public async Task<IEnumerable<DoorDto>> BrowseAsync(Guid userId)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var rooms = await _roomRepository.GetRoomsForUser(user.Id);
            var doors = rooms.SelectMany(r => r.Doors);
            return _mapper.Map<IEnumerable<DoorDto>>(doors);
        }

        public async Task<IEnumerable<DoorDto>> BrowseForRoomAsync(Guid userId, Guid roomId)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var room = await _roomRepository.GetSingleRoomForUser(user.Id, roomId);
            return _mapper.Map<IEnumerable<DoorDto>>(room.Doors);
        }

        public async Task<DoorDto> GetSingleDoorAsync(Guid userId, string firstPlace, string secondPlace)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var rooms = await _roomRepository.GetRoomsForUser(user.Id);
            var door = rooms.SelectMany(r => r.Doors).SingleOrDefault(d => d.ConnectedPlaces[0] == firstPlace && d.ConnectedPlaces[1] == secondPlace);
            return _mapper.Map<DoorDto>(door);
        }

        public async Task RemoveDoorAsync(Guid userId, Guid roomId, string firstPlace, string secondPlace)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var room = await _roomRepository.GetSingleRoomForUser(user.Id, roomId);
            var door = room.Doors.SingleOrDefault(d => d.ConnectedPlaces[0] == firstPlace && d.ConnectedPlaces[1] == secondPlace);
            room.RemoveDoor(door);
            await _roomRepository.UpdateAsync(room);
        }

        public async Task UpdateDoorAsync(Guid roomId, string firstPlace, string secondPlace)
        {
            throw new NotImplementedException();
        }
    }
}