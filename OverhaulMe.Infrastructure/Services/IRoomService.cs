﻿using OverhaulMe.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Services
{
    public interface IRoomService : IService
    {
        Task<RoomDetailsDto> GetAsync(Guid userId, Guid roomId);
        Task<IEnumerable<RoomDto>> BrowseAsync(Guid userId);
        Task CreateRoomAsync(Guid userId, Guid roomId, string roomName, float floorSize, float wallSize, float ceilingSize);
        Task DeleteAsync(Guid userId, Guid roomId);
    }
}