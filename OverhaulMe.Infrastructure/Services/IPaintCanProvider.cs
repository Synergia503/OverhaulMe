﻿using OverhaulMe.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Services
{
    public interface IPaintCanProvider : IService
    {
        Task<IList<PaintCanDto>> GetByBrandNameAsync(Guid userId, string brandName);
        Task<IEnumerable<PaintCanDto>> BrowseForRoomAsync(Guid userId, Guid roomId);
        Task<IEnumerable<PaintCanDto>> BrowseAllAsync(Guid userId);
        Task AddPaintCanAsync(Guid userId, Guid roomId, string colour, string brandName, float boughtVolume, float remainingVolume, decimal price, DateTime purchasedAt, DateTime? paintedAt = null);
        Task RemovePaintCanAsync(Guid userId, Guid roomId, int canNumbersToRemove, string brandName);
        Task UpdatePaintCanAsync(Guid roomId);
    }
}