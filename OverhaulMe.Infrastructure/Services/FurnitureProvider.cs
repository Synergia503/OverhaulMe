﻿using AutoMapper;
using OverhaulMe.Core.Domain;
using OverhaulMe.Core.Repositories;
using OverhaulMe.Infrastructure.DTO;
using OverhaulMe.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Services
{
    public class FurnitureProvider : IFurnitureProvider
    {
        private readonly IRoomRepository _roomRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public FurnitureProvider(IRoomRepository roomRepository, IUserRepository userRepository, IMapper mapper)
        {
            _roomRepository = roomRepository;
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task AddFurnitureAsync(Guid userId, Guid roomId, Guid furnitureId, string name, decimal price, string place, string purpose, string boughtPlace, bool isNewPurchased, DateTime purchasedAt, DateTime? mountedAt)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var room = await _roomRepository.GetSingleRoomForUser(user.Id, roomId);
            var furniture = FurnitureFluentFactory.Init(new Furniture())
                .AddPropertyValue(x => x.RoomId, roomId)
                .AddPropertyValue(x => x.Id, furnitureId)
                .AddPropertyValue(x => x.Name, name)
                .AddPropertyValue(x => x.Price, price)
                .AddPropertyValue(x => x.Place, place)
                .AddPropertyValue(x => x.Purpose, purpose)
                .AddPropertyValue(x => x.BoughtPlace, boughtPlace)
                .AddPropertyValue(x => x.IsNewPurchased, isNewPurchased)
                .AddPropertyValue(x => x.PurchasedAt, purchasedAt)
                .AddPropertyValue(x => x.MountedAt, mountedAt)
                .Create();

            room.AddFurniture(furniture);
            await _roomRepository.UpdateAsync(room);
        }

        public async Task<IEnumerable<FurnitureDto>> BrowseAsync(Guid userId)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var rooms = await _roomRepository.GetRoomsForUser(user.Id);
            var furnitures = rooms.SelectMany(r => r.Furnitures);
            return _mapper.Map<IEnumerable<FurnitureDto>>(furnitures);
        }

        public async Task<IEnumerable<FurnitureDto>> BrowseForRoomAsync(Guid userId, Guid roomId)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var room = await _roomRepository.GetSingleRoomForUser(user.Id, roomId);
            return _mapper.Map<IEnumerable<FurnitureDto>>(room.Furnitures);
        }

        public async Task<FurnitureDto> GetFurnitureAsync(Guid userId, Guid furnitureId)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var rooms = await _roomRepository.GetRoomsForUser(user.Id);
            var furniture = rooms.SelectMany(r => r.Furnitures).SingleOrDefault(f => f.Id == furnitureId);
            return _mapper.Map<FurnitureDto>(furniture);
        }

        public async Task RemoveFurnitureAsync(Guid userId, Guid roomId, Guid furnitureId)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var room = await _roomRepository.GetSingleRoomForUser(user.Id, roomId);
            var furniture = room.Furnitures.SingleOrDefault(f => f.Id == furnitureId);
            room.RemoveFurniture(furniture);
            await _roomRepository.UpdateAsync(room);
        }

        public async Task UpdateFurnitureAsync(Guid roomId, Guid furnitureId)
        {
            throw new NotImplementedException();
        }
    }
}