﻿using AutoMapper;
using OverhaulMe.Core.Domain;
using OverhaulMe.Core.Repositories;
using OverhaulMe.Infrastructure.DTO;
using OverhaulMe.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Services
{
    public class PaintCanProvider : IPaintCanProvider
    {
        private readonly IRoomRepository _roomRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public PaintCanProvider(IRoomRepository roomRepository, IUserRepository userRepository, IMapper mapper)
        {
            _roomRepository = roomRepository;
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task AddPaintCanAsync(Guid userId, Guid roomId, string colour, string brandName, float boughtVolume, float remainingVolume, decimal price, DateTime purchasedAt, DateTime? paintedAt = null)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var room = await _roomRepository.GetSingleRoomForUser(user.Id, roomId);
            var paintCan = PaintCan.Create(room, colour, brandName, boughtVolume, remainingVolume, price, purchasedAt, null);
            room.AddPaintCan(paintCan);
            await _roomRepository.UpdateAsync(room);
        }

        public async Task<IEnumerable<PaintCanDto>> BrowseAllAsync(Guid userId)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var rooms = await _roomRepository.GetRoomsForUser(user.Id);
            var paintCans = rooms.SelectMany(r => r.PaintCans);
            return _mapper.Map<IEnumerable<PaintCanDto>>(paintCans);
        }

        public async Task<IEnumerable<PaintCanDto>> BrowseForRoomAsync(Guid userId, Guid roomId)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var room = await _roomRepository.GetSingleRoomForUser(user.Id, roomId);
            return _mapper.Map<IEnumerable<PaintCanDto>>(room.PaintCans);
        }

        public async Task<IList<PaintCanDto>> GetByBrandNameAsync(Guid userId, string brandName)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var rooms = await _roomRepository.GetRoomsForUser(user.Id);
            var paintCans = rooms.SelectMany(r => r.PaintCans).Where(pc => pc.BrandName == brandName);
            return _mapper.Map<IList<PaintCanDto>>(paintCans);
        }

        public async Task RemovePaintCanAsync(Guid userId, Guid roomId, int canNumbersToRemove, string brandName)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var room = await _roomRepository.GetSingleRoomForUser(user.Id, roomId);
            room.RemovePaintCans(canNumbersToRemove, brandName);
            await _roomRepository.UpdateAsync(room);
        }

        public async Task UpdatePaintCanAsync(Guid roomId)
        {
            throw new NotImplementedException();
        }
    }
}