﻿using System;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Services.DapperServices
{
    public interface IDatabaseConnectionService : IDisposable
    {
        Task<SqlConnection> CreateConnectionAsync();
        SqlConnection CreateConnection();
    }
}