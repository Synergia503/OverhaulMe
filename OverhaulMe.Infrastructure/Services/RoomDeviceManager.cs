﻿using AutoMapper;
using OverhaulMe.Core.Domain;
using OverhaulMe.Core.Repositories;
using OverhaulMe.Infrastructure.DTO;
using OverhaulMe.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Services
{
    public class RoomDeviceManager : IRoomDeviceManager
    {
        private readonly IRoomRepository _roomRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public RoomDeviceManager(IRoomRepository roomRepository, IUserRepository userRepository, IMapper mapper)
        {
            _roomRepository = roomRepository;
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<DeviceDto> GetDeviceAsync(Guid userId, Guid deviceId)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var rooms = await _roomRepository.GetRoomsForUser(user.Id);
            var device = rooms.SelectMany(d => d.Devices).SingleOrDefault(x => x.Id == deviceId);
            return _mapper.Map<DeviceDto>(device);
        }

        public async Task<IEnumerable<DeviceDto>> BrowseAsync(Guid userId)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var rooms = await _roomRepository.GetRoomsForUser(user.Id);
            var devices = rooms.SelectMany(x => x.Devices);
            return _mapper.Map<IEnumerable<DeviceDto>>(devices);
        }

        public async Task<IEnumerable<DeviceDto>> BrowseForRoomAsync(Guid userId, Guid roomId)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var room = await _roomRepository.GetSingleRoomForUser(user.Id, roomId);
            var devices = room?.Devices;
            return _mapper.Map<IEnumerable<DeviceDto>>(devices);
        }

        public async Task CreateDeviceAsync(Guid userId, Guid roomId, Guid deviceId, string deviceName, decimal price, bool isNewPurchased, DateTime? purchasedAt = null)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var room = await _roomRepository.GetSingleRoomForUser(user.Id, roomId);
            var device = Device.Create(room, deviceId, deviceName, price, isNewPurchased, purchasedAt);
            room.AddDevice(device);
            await _roomRepository.UpdateAsync(room);
        }

        public async Task RemoveDeviceAsync(Guid userId, Guid roomId, Guid deviceId)
        {
            var user = await _userRepository.GetUserByIdOrFailAsync(userId);
            var room = await _roomRepository.GetSingleRoomForUser(user.Id, roomId);
            var device = room.Devices.SingleOrDefault(d => d.Id == deviceId);
            room.RemoveDevice(device);
            await _roomRepository.UpdateAsync(room);
        }

        public async Task UpdateDeviceAsync(Guid roomId, Guid deviceId)
        {
            throw new NotImplementedException();
        }
    }
}