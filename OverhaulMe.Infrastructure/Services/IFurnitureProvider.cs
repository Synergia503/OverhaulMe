﻿using OverhaulMe.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Services
{
    public interface IFurnitureProvider : IService
    {
        Task AddFurnitureAsync(Guid userId, Guid roomId, Guid furnitureId, string name, decimal price, string place, string purpose, string boughtPlace, bool isNewPurchased, DateTime purchasedAt, DateTime? mountedAt);
        Task<FurnitureDto> GetFurnitureAsync(Guid userId, Guid furnitureId);
        Task<IEnumerable<FurnitureDto>> BrowseAsync(Guid userId);
        Task<IEnumerable<FurnitureDto>> BrowseForRoomAsync(Guid userId, Guid roomId);
        Task RemoveFurnitureAsync(Guid userId, Guid roomId, Guid furnitureId);
        Task UpdateFurnitureAsync(Guid roomId, Guid furnitureId);
    }
}