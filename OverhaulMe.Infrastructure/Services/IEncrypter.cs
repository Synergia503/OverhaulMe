﻿namespace OverhaulMe.Infrastructure.Services
{
    public interface IEncrypter : IService
    {
        string GetSalt();
        string GetHash(string value, string salt);
    }
}