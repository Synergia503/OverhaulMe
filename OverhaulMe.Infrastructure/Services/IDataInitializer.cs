﻿using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Services
{
    public interface IDataInitializer : IService
    {
        Task SeedRandomDataAsync();
        Task SeedSpecificDataAsync();
    }
}