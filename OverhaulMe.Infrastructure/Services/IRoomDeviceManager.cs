﻿using OverhaulMe.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Services
{
    public interface IRoomDeviceManager : IService
    {
        Task<DeviceDto> GetDeviceAsync(Guid userId, Guid deviceId);
        Task<IEnumerable<DeviceDto>> BrowseAsync(Guid userId);
        Task<IEnumerable<DeviceDto>> BrowseForRoomAsync(Guid userId, Guid roomId);
        Task CreateDeviceAsync(Guid userId, Guid roomId, Guid deviceId, string deviceName, decimal price, bool isNewPurchased, DateTime? purchasedAt = null);
        Task RemoveDeviceAsync(Guid userId, Guid roomId, Guid deviceId);
        Task UpdateDeviceAsync(Guid roomId, Guid deviceId);
    }
}