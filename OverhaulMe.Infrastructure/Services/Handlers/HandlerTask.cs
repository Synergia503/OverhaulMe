﻿using OverhaulMe.Core.DomainExceptions;
using System;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Services.Handlers
{
    public class HandlerTask : IHandlerTask
    {
        private readonly IHandler _handler;
        private readonly Func<Task> _runAsync;
        private Func<Task> _validateAsync;
        private Func<Task> _alwaysAsync;
        private Func<Task> _onSuccessAsync;
        private Func<Exception, Task> _onErrorAsync;
        private Func<IDomainException, Task> _onCustomErrorAsync;
        private bool _propagateException = true;
        private bool _executeOnError = true;

        public HandlerTask(IHandler handler, Func<Task> runAsync, Func<Task> validateAsync = null)
        {
            _handler = handler;
            _runAsync = runAsync;
            _validateAsync = validateAsync;
        }

        public IHandlerTask Always(Func<Task> always)
        {
            _alwaysAsync = always;
            return this;
        }

        public IHandlerTask DoNotPropagateException()
        {
            _propagateException = false;
            return this;
        }

        public async Task ExecuteAsync()
        {
            try
            {
                if (_validateAsync != null)
                {
                    await _validateAsync();
                }

                await _runAsync();
                if (_onSuccessAsync != null)
                {
                    await _onSuccessAsync();
                }
            }
            catch (Exception exception)
            {
                await HandleExceptionAsync(exception);
                if (_propagateException)
                {
                    throw;
                }
            }
            finally
            {
                if (_alwaysAsync != null)
                {
                    await _alwaysAsync();
                }
            }
        }

        private async Task HandleExceptionAsync(Exception exception)
        {
            var customException = exception as IDomainException;
            if (customException != null && _onCustomErrorAsync != null)
            {
                await _onCustomErrorAsync(customException);
            }

            var executeOnError = _executeOnError || customException == null;
            if (executeOnError && _onErrorAsync != null)
            {
                await _onErrorAsync(exception);
            }
        }

        public IHandler Next() => _handler;

        public IHandlerTask OnCustomError(Func<IDomainException, Task> onCustomError, bool propagateException = false, bool executeOnError = false)
        {
            _onCustomErrorAsync = onCustomError;
            _propagateException = propagateException;
            _executeOnError = executeOnError;
            return this;
        }

        public IHandlerTask OnError(Func<Exception, Task> onError, bool propagateException = false)
        {
            _onErrorAsync = onError;
            _propagateException = propagateException;
            return this;
        }

        public IHandlerTask OnSuccess(Func<Task> onSuccess)
        {
            _onSuccessAsync = onSuccess;
            return this;
        }

        public IHandlerTask PropagateException()
        {
            _propagateException = true;
            return this;
        }
    }
}