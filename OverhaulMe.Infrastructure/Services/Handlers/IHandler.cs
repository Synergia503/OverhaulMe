﻿using System;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Services.Handlers
{
    public interface IHandler : IService
    {
        IHandlerTask Run(Func<Task> runAsync);
        IHandlerTaskRunner Validate(Func<Task> validateAsync);
        Task ExecuteAllAsync();
    }
}