﻿using System;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Services.Handlers
{
    public interface IHandlerTaskRunner
    {
        IHandlerTask Run(Func<Task> runAsync);
    }
}