﻿using OverhaulMe.Core.Repositories;
using OverhaulMe.Infrastructure.Extensions;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Services
{
    public class DataInitializer : IDataInitializer
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserService _userService;
        private readonly IRoomService _roomService;
        private readonly IRoomDeviceManager _roomDeviceManager;
        private readonly IDuctingProvider _ductingProvider;
        private readonly IFurnitureProvider _furnitureProvider;
        private readonly IWindowProvider _windowProvider;
        private readonly IDoorProvider _doorProvider;
        private readonly IPaintCanProvider _paintCanProvider;
        private static readonly object syncLock = new object();
        private static readonly Random random = new Random();

        public DataInitializer(
            IUserService userService, IRoomService roomService, IRoomDeviceManager roomDeviceManager, IDuctingProvider ductingProvider,
            IFurnitureProvider furnitureProvider, IWindowProvider windowProvider, IDoorProvider doorProvider, IPaintCanProvider paintCanProvider)
        {
            _userService = userService;
            _roomService = roomService;
            _roomDeviceManager = roomDeviceManager;
            _ductingProvider = ductingProvider;
            _furnitureProvider = furnitureProvider;
            _windowProvider = windowProvider;
            _doorProvider = doorProvider;
            _paintCanProvider = paintCanProvider;
        }

        public async Task SeedRandomDataAsync()
        {
            var users = await _userService.BrowseAsync();
            if (users.Any())
            {
                return;
            }

            for (int i = 1; i <= 10; i++)
            {
                var userId = Guid.NewGuid();

                if (i % 3 == 0)
                {
                    var adminName = $"admin{i}";
                    var fullAdminName = $"Test Admin Full Name{i}";
                    await _userService.RegisterAsync(userId, $"{adminName}@test.com", adminName, fullAdminName, $"super_secret{i}", "admin");
                }
                else
                {
                    var username = $"user{i}";
                    var fullName = $"Test User Full Name{i}";
                    await _userService.RegisterAsync(userId, $"{username}@test.com", username, fullName, $"secret{i}", "user");
                }

                for (int j = 1; j <= 5; j++)
                {
                    var roomId = Guid.NewGuid();
                    var roomName = $"Room{j}";
                    await _roomService.CreateRoomAsync(userId, roomId, roomName, j + 10f, (j + 10f) * 4f, j + 9f);

                    for (int k = 1; k <= 3; k++)
                    {
                        var deviceName = $"Device{j}-{k}";
                        var randomPrice = RandomNumberInt(10, 4000);
                        var doubleRandom = RandomNumberDouble();
                        doubleRandom = Math.Round(doubleRandom, 2);
                        await _roomDeviceManager.CreateDeviceAsync(userId, roomId, Guid.NewGuid(), deviceName, (decimal)(randomPrice * doubleRandom), k % 2 == 1, DateTime.UtcNow.Date);
                        var ductingType = $"Type{j}-{k}";
                        await _ductingProvider.AddDuctingAsync(userId, roomId, ductingType, k * 100, k * 250, roomName, $"Test shop-{k}", DateTime.UtcNow, DateTime.UtcNow.AddDays(10 + k));
                        var furnitureName = $"Furniture{j}-{k}";
                        await _furnitureProvider.AddFurnitureAsync(userId, roomId, Guid.NewGuid(), furnitureName, k * 250, roomName, $"Test purpose-{k}", $"Test shop-{k}", k % 2 == 1, DateTime.UtcNow, DateTime.UtcNow.AddDays(5));
                        var windowBrandName = $"WindowBrandName{j}";
                        await _windowProvider.AddWindowAsync(userId, roomId, Guid.NewGuid(), roomName, k * 25f, k * 5f, k * 0.5f, k * 300, windowBrandName, $"Test window kind-{k}", k % 2 == 1, DateTime.UtcNow, DateTime.UtcNow.AddDays(k));
                        var doorBrandName = $"DoorBrandName{j}";
                        await _doorProvider.AddDoorAsync(userId, roomId, $"Room{k - 1}", k * 50, k * 10, k * 0.2f, k * 500, doorBrandName, $"Test door kind-{k}", k % 2 == 1, 5, assembledAt: DateTime.UtcNow);
                        var randomRemainingPaintVolume = RandomNumberDouble() * 100;
                        randomRemainingPaintVolume = Math.Round(randomRemainingPaintVolume, 2);
                        var randomBoughtPaintVolume = (float)Math.Ceiling(randomRemainingPaintVolume) + k * 10f;
                        await _paintCanProvider.AddPaintCanAsync(userId, roomId, $"Colour{k}", $"BrandName{k + 1}", randomBoughtPaintVolume, (float)randomRemainingPaintVolume, k * 25m, DateTime.UtcNow.AddDays(-200), DateTime.UtcNow);
                    }
                }
            }
        }

        public async Task SeedSpecificDataAsync()
        {
            var users = await _userService.BrowseAsync();
            var userIdForTest = Guid.Parse("00000000-0000-0000-0000-000000000000");

            if (users.Any(u => u.Id == userIdForTest))
            {
                return;
            }

            await _userService.RegisterAsync(userIdForTest, "test@test.com", "TestName", "TestFullName", "secret", "user");

            var roomIdForTest = Guid.Parse("11111111-aaaa-1111-1111-111111aaaaaa");
            var deviceIdForTest1 = Guid.Parse("22222222-bbbb-1111-1111-111111aaaaaa");
            var deviceIdForTest2 = Guid.Parse("22222222-cccc-1111-1111-111111aaaaaa");
            var deviceIdForTest3 = Guid.Parse("22222222-dddd-1111-1111-111111aaaaaa");
            var deviceIdForTest4 = Guid.Parse("22222222-eeee-1111-1111-111111aaaaaa");
            var deviceIdForTest5 = Guid.Parse("22222222-ffff-1111-1111-111111aaaaaa");
            var furnitureIdForTest1 = Guid.Parse("33333333-aaaa-1111-1111-111111aaaaaa");
            var furnitureIdForTest2 = Guid.Parse("33333333-bbbb-1111-1111-111111aaaaaa");
            var furnitureIdForTest3 = Guid.Parse("33333333-cccc-1111-1111-111111aaaaaa");
            var furnitureIdForTest4 = Guid.Parse("33333333-dddd-1111-1111-111111aaaaaa");
            var furnitureIdForTest5 = Guid.Parse("33333333-eeee-1111-1111-111111aaaaaa");
            var furnitureIdForTest6 = Guid.Parse("33333333-ffff-1111-1111-111111aaaaaa");
            var furnitureIdForTest7 = Guid.Parse("33333333-aaaa-2222-1111-111111aaaaaa");
            var windowIdForTest1 = Guid.Parse("44444444-aaaa-1111-1111-111111aaaaaa");
            var windowIdForTest2 = Guid.Parse("44444444-bbbb-1111-1111-111111aaaaaa");
            var windowIdForTest3 = Guid.Parse("44444444-cccc-1111-1111-111111aaaaaa");
            var windowIdForTest4 = Guid.Parse("44444444-dddd-1111-1111-111111aaaaaa");

            var rooms = await _roomService.BrowseAsync(userIdForTest);
            if (!rooms.Any(x => x.UserId == userIdForTest))
            {
                var testRoomName = "TestRoom1";
                await _roomService.CreateRoomAsync(userIdForTest, roomIdForTest, testRoomName, 1f, 2f, 3f);
                await _roomDeviceManager.CreateDeviceAsync(userIdForTest, roomIdForTest, deviceIdForTest1, "Test device1", 10m, true, DateTime.UtcNow);
                await _roomDeviceManager.CreateDeviceAsync(userIdForTest, roomIdForTest, deviceIdForTest2, "Test device2", 20m, false, DateTime.UtcNow);
                await _roomDeviceManager.CreateDeviceAsync(userIdForTest, roomIdForTest, deviceIdForTest3, "Test device3", 30m, true, null);
                await _roomDeviceManager.CreateDeviceAsync(userIdForTest, roomIdForTest, deviceIdForTest4, "Test device4", 40m, false, null);
                await _roomDeviceManager.CreateDeviceAsync(userIdForTest, roomIdForTest, deviceIdForTest5, "Test device5", 50m, true, DateTime.UtcNow.AddDays(-10));
                await _ductingProvider.AddDuctingAsync(userIdForTest, roomIdForTest, "TestType1", 100f, 250m, testRoomName, $"Test shop-1", DateTime.UtcNow.AddDays(-10), DateTime.UtcNow);
                await _ductingProvider.AddDuctingAsync(userIdForTest, roomIdForTest, "TestType2", 100f, 250m, testRoomName, $"Test shop-1", DateTime.UtcNow.AddDays(-10), DateTime.UtcNow);
                await _ductingProvider.AddDuctingAsync(userIdForTest, roomIdForTest, "TestType3", 100f, 250m, testRoomName, $"Test shop-2", DateTime.UtcNow.AddDays(-10), DateTime.UtcNow);
                await _ductingProvider.AddDuctingAsync(userIdForTest, roomIdForTest, "TestType11", 100f, 250m, testRoomName, $"Test shop-2", DateTime.UtcNow.AddDays(-10), null);
                await _furnitureProvider.AddFurnitureAsync(userIdForTest, roomIdForTest, furnitureIdForTest1, "TestFurniture1", 100m, testRoomName, "Leisure", "Test shop-1", true, DateTime.UtcNow.AddDays(-10), DateTime.UtcNow);
                await _furnitureProvider.AddFurnitureAsync(userIdForTest, roomIdForTest, furnitureIdForTest2, "TestFurniture2", 100m, testRoomName, "Rest", "Test shop-1", true, DateTime.UtcNow.AddDays(-100), DateTime.UtcNow);
                await _furnitureProvider.AddFurnitureAsync(userIdForTest, roomIdForTest, furnitureIdForTest3, "TestFurniture3", 100m, testRoomName, "Wardrobe", "Test shop-1", true, DateTime.UtcNow.AddDays(-45), DateTime.UtcNow);
                await _furnitureProvider.AddFurnitureAsync(userIdForTest, roomIdForTest, furnitureIdForTest4, "TestFurniture4", 100m, testRoomName, "Wardrobe", "Test shop-1", true, DateTime.UtcNow.AddDays(-12), DateTime.UtcNow);
                await _furnitureProvider.AddFurnitureAsync(userIdForTest, roomIdForTest, furnitureIdForTest5, "TestFurniture5", 100m, testRoomName, "Rest", "Test shop-1", true, DateTime.UtcNow.AddDays(-19), DateTime.UtcNow);
                await _furnitureProvider.AddFurnitureAsync(userIdForTest, roomIdForTest, furnitureIdForTest6, "TestFurniture6", 100m, testRoomName, "Wardrobe", "Test shop-1", true, DateTime.UtcNow.AddDays(-1), DateTime.UtcNow);
                await _furnitureProvider.AddFurnitureAsync(userIdForTest, roomIdForTest, furnitureIdForTest7, "TestFurniture7", 100m, testRoomName, "Leisure", "Test shop-1", true, DateTime.UtcNow.AddDays(-55), DateTime.UtcNow);
                await _windowProvider.AddWindowAsync(userIdForTest, roomIdForTest, windowIdForTest1, testRoomName, 100f, 20f, 5f, 500m, "TestBrand1", "TestKind1", true, DateTime.UtcNow.AddDays(-50), DateTime.UtcNow);
                await _windowProvider.AddWindowAsync(userIdForTest, roomIdForTest, windowIdForTest2, testRoomName, 100f, 20f, 5f, 500m, "TestBrand1", "TestKind1", true, DateTime.UtcNow.AddDays(-50), null);
                await _windowProvider.AddWindowAsync(userIdForTest, roomIdForTest, windowIdForTest3, testRoomName, 100f, 20f, 5f, 500m, "TestBrand1", "TestKind2", true, DateTime.UtcNow.AddDays(-50), DateTime.UtcNow);
                await _windowProvider.AddWindowAsync(userIdForTest, roomIdForTest, windowIdForTest4, testRoomName, 100f, 20f, 5f, 500m, "TestBrand1", "TestKind3", true, DateTime.UtcNow.AddDays(-50), null);
                await _doorProvider.AddDoorAsync(userIdForTest, roomIdForTest, "Room2", 220f, 70f, 35f, 600m, "TestBrand1", "TestKind1", false, 25.5f, null, DateTime.UtcNow.AddDays(-10), DateTime.UtcNow.AddDays(-50), DateTime.UtcNow.AddDays(-20));
                await _doorProvider.AddDoorAsync(userIdForTest, roomIdForTest, "Room1", 210f, 60f, 20f, 950m, "TestBrand4", "TestKind4", true, 5.5f, DateTime.UtcNow.AddDays(-50), DateTime.UtcNow.AddDays(-10), DateTime.UtcNow.AddDays(-10), DateTime.UtcNow.AddDays(-10));
                await _doorProvider.AddDoorAsync(userIdForTest, roomIdForTest, "Room6", 200f, 80f, 30f, 850m, "TestBrand1", "TestKind1", true, 7.5f, DateTime.UtcNow.AddDays(-50), DateTime.UtcNow.AddDays(-10), DateTime.UtcNow.AddDays(-10), DateTime.UtcNow.AddDays(-10));
                await _doorProvider.AddDoorAsync(userIdForTest, roomIdForTest, "Room4", 205f, 60f, 30f, 550m, "TestBrand5", "TestKind2", false, 35.5f, null, DateTime.UtcNow.AddDays(-5), DateTime.UtcNow.AddDays(-100), DateTime.UtcNow.AddDays(-25));
                await _paintCanProvider.AddPaintCanAsync(userIdForTest, roomIdForTest, "TestColour1", "TestBrand1", 40f, 35f, 90m, DateTime.UtcNow.AddDays(-100), DateTime.UtcNow.AddDays(-1));
                await _paintCanProvider.AddPaintCanAsync(userIdForTest, roomIdForTest, "TestColour2", "TestBrand1", 50f, 0f, 70m, DateTime.UtcNow.AddDays(-10), null);
                await _paintCanProvider.AddPaintCanAsync(userIdForTest, roomIdForTest, "TestColour3", "TestBrand1", 30f, 3f, 55m, DateTime.UtcNow.AddDays(-20), DateTime.UtcNow.AddDays(-2));
                await _paintCanProvider.AddPaintCanAsync(userIdForTest, roomIdForTest, "TestColour4", "TestBrand1", 20f, 5.6f, 89.99m, DateTime.UtcNow.AddDays(-45), DateTime.UtcNow.AddDays(-15));
                await _paintCanProvider.AddPaintCanAsync(userIdForTest, roomIdForTest, "TestColour5", "TestBrand2", 60f, 3.5f, 10.99m, DateTime.UtcNow.AddDays(-54), null);
                await _paintCanProvider.AddPaintCanAsync(userIdForTest, roomIdForTest, "TestColour6", "TestBrand2", 50f, 50f, 15.99m, DateTime.UtcNow.AddDays(-55), DateTime.UtcNow.AddDays(-45));
                await _paintCanProvider.AddPaintCanAsync(userIdForTest, roomIdForTest, "TestColour7", "TestBrand3", 50f, 28f, 19.90m, DateTime.UtcNow.AddDays(-76), DateTime.UtcNow.AddDays(-7));
                await _paintCanProvider.AddPaintCanAsync(userIdForTest, roomIdForTest, "TestColour8", "TestBrand3", 40f, 15.6f, 29.90m, DateTime.UtcNow.AddDays(-123), DateTime.UtcNow.AddDays(-6));
                await _paintCanProvider.AddPaintCanAsync(userIdForTest, roomIdForTest, "TestColour9", "TestBrand3", 40f, 28.8f, 39.89m, DateTime.UtcNow.AddDays(-19), null);
                await _paintCanProvider.AddPaintCanAsync(userIdForTest, roomIdForTest, "TestColour1", "TestBrand3", 30f, 11f, 9.99m, DateTime.UtcNow.AddDays(-46), DateTime.UtcNow.AddDays(-39));
                await _paintCanProvider.AddPaintCanAsync(userIdForTest, roomIdForTest, "TestColour2", "TestBrand4", 30f, 22.6f, 20.94m, DateTime.UtcNow.AddDays(-50), DateTime.UtcNow.AddDays(-8));
                await _paintCanProvider.AddPaintCanAsync(userIdForTest, roomIdForTest, "TestColour3", "TestBrand5", 30f, 3f, 31.12m, DateTime.UtcNow.AddDays(-45), DateTime.UtcNow.AddDays(-5));
                await _paintCanProvider.AddPaintCanAsync(userIdForTest, roomIdForTest, "TestColour5", "TestBrand8", 30f, 30f, 23.33m, DateTime.UtcNow.AddDays(-45), null);
            }
        }

        private static int RandomNumberInt(int min, int max)
        {
            lock (syncLock)
            { // synchronize
                return random.Next(min, max);
            }
        }

        private static double RandomNumberDouble()
        {
            lock (syncLock)
            { // synchronize
                return random.NextDouble();
            }
        }
    }
}