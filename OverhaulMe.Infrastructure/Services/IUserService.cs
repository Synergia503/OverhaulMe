﻿using OverhaulMe.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Services
{
    public interface IUserService : IService
    {
        Task<UserDto> GetAsync(string email);
        Task RegisterAsync(Guid userId, string email, string username, string fullName, string password, string role);
        Task LoginAsync(string email, string password);
        Task<IEnumerable<UserDto>> BrowseAsync();
    }
}