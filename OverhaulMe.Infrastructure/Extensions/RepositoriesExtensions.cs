﻿using OverhaulMe.Core.Domain;
using OverhaulMe.Core.DomainExceptions;
using OverhaulMe.Core.Repositories;
using System;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Extensions
{
    public static class RepositoriesExtensions
    {
        public static async Task<User> GetUserByIdOrFailAsync(this IUserRepository repository, Guid userId)
        {
            var user = await repository.GetAsync(userId);
            if (user == null)
            {
                throw new UserException(ErrorCodes.UserNotExists, $"User with id: {userId} does not exist.");
            }

            return user;
        }
    }
}