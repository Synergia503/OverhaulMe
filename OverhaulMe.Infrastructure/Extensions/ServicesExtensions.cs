﻿namespace OverhaulMe.Infrastructure.Extensions
{
    public static class ServicesExtensions
    {
        public static string FirstLetterToUpperCaseOrConvertNullToEmptyString(this string inputString)
        {
            if (string.IsNullOrEmpty(inputString))
            {
                return string.Empty;
            }

            char[] charArrary = inputString.ToCharArray();
            charArrary[0] = char.ToUpper(charArrary[0]);
            return new string(charArrary);
        }
    }
}