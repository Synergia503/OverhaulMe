﻿using System;

namespace OverhaulMe.Infrastructure.DTO
{
    public class DuctingDto
    {
        public Guid RoomId { get; set; }
        public string Type { get; set; }
        public float Length { get; set; }
        public decimal Price { get; set; }
        public string Place { get; set; }
        public string BoughtPlace { get; set; }
        public DateTime PurchasedAt { get; set; }
        public DateTime? AssembledAt { get; set; }
    }
}