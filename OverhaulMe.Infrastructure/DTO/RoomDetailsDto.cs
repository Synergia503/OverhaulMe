﻿using System.Collections.Generic;

namespace OverhaulMe.Infrastructure.DTO
{
    public class RoomDetailsDto : RoomDto
    {
        public IEnumerable<DeviceDto> Devices { get; set; }
        public decimal DevicesPrice { get; set; }
        public int DevicesCount { get; set; }
        public int NewDevicesCount { get; set; }
        public IEnumerable<DuctingDto> Ductings { get; set; }
        public decimal DuctingsPrice { get; set; }
        public IEnumerable<string> DuctingsTypes { get; set; }
        public IEnumerable<FurnitureDto> Furnitures { get; set; }
        public int FurnituresCount { get; set; }
        public int NewFurnituresCount { get; set; }
        public decimal FurnituresPrice { get; set; }
        public IEnumerable<WindowDto> Windows { get; set; }
        public int WindowsCount { get; set; }
        public int NewWindowsCount { get; set; }
        public IEnumerable<DoorDto> Doors { get; set; }
        public int DoorsCount { get; set; }
        public int NewDoorsCount { get; set; }
        public IEnumerable<PaintCanDto> PaintCans { get; set; }
        public IEnumerable<string> Brands { get; set; }
        public float PaintSpentVolume { get; set; }
        public float PaintBoughtVolume { get; set; }
        public float PaintRemainingVolume { get; set; }
    }
}