﻿using System;

namespace OverhaulMe.Infrastructure.DTO
{
    public class RoomDto
    {
        public Guid UserId { get; set; }
        public Guid Id { get; set; }
        public string Name { get; set; }
        public float FloorSize { get; set; }
        public float WallSize { get; set; }
        public float CeilingSize { get; set; }
    }
}