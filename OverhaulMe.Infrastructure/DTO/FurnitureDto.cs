﻿using System;

namespace OverhaulMe.Infrastructure.DTO
{
    public class FurnitureDto
    {
        public Guid FurnitureId { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Place { get; set; }
        public string Purpose { get; set; }
        public string BoughtPlace { get; set; }
        public bool IsNewPurchased { get; set; }
        public DateTime PurchasedAt { get; set; }
        public DateTime? MountedAt { get; set; }
    }
}