﻿using System;

namespace OverhaulMe.Infrastructure.DTO
{
    public class WindowDto
    {
        public Guid WindowId { get; set; }
        public string Place { get; set; }
        public float Height { get; set; }
        public float Width { get; set; }
        public float Depth { get; set; }
        public decimal Price { get; set; }
        public string BrandName { get; set; }
        public string Kind { get; set; }
        public bool IsNewPurchased { get; set; }
        public DateTime PurchasedAt { get; set; }
        public DateTime? AssembledAt { get; set; }
    }
}