﻿using System;

namespace OverhaulMe.Infrastructure.DTO
{
    public class DeviceDto
    {
        public Guid DeviceId { get; set; }
        public string DeviceName { get; set; }
        public decimal Price { get; set; }
        public string Place { get; set; }
        public bool IsNewPurchased { get; set; }
        public DateTime? PurchasedAt { get; set; }
    }
}