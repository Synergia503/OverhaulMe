﻿using Autofac;
using OverhaulMe.Infrastructure.Services;
using OverhaulMe.Infrastructure.Services.DapperServices;
using OverhaulMe.Infrastructure.Services.Jwt;
using System.Reflection;

namespace OverhaulMe.Infrastructure.IoC.Modules
{
    public class ServiceModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var assembly = typeof(ServiceModule)
                .GetTypeInfo()
                .Assembly;

            builder.RegisterAssemblyTypes(assembly)
               .Where(x => x.IsAssignableTo<IService>())
               .AsImplementedInterfaces()
               .InstancePerLifetimeScope();

            builder.RegisterType<Encrypter>()
              .As<IEncrypter>()
              .SingleInstance();

            builder.RegisterType<JwtHandler>()
                .As<IJwtHandler>()
                .SingleInstance();
        }
    }
}