﻿using Autofac;
using Microsoft.Extensions.Configuration;
using OverhaulMe.Infrastructure.DapperRepositories;
using OverhaulMe.Infrastructure.Extensions;
using OverhaulMe.Infrastructure.Settings;

namespace OverhaulMe.Infrastructure.IoC.Modules
{
    public class SettingsModule : Autofac.Module
    {
        private readonly IConfiguration _configuration;
        public SettingsModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterInstance(_configuration.GetSettings<GeneralSettings>())
               .SingleInstance();
            builder.RegisterInstance(_configuration.GetSettings<JwtSettings>())
               .SingleInstance();
            builder.RegisterInstance(_configuration.GetSettings<SqlSettings>())
               .SingleInstance();
        }
    }
}