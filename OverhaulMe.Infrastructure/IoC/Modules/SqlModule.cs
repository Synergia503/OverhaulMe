﻿using Autofac;
using Microsoft.Extensions.Configuration;
using OverhaulMe.Infrastructure.DapperRepositories;
using OverhaulMe.Infrastructure.Extensions;
using OverhaulMe.Infrastructure.Services.DapperServices;
using System.Reflection;

namespace OverhaulMe.Infrastructure.IoC.Modules
{
    public class SqlModule : Autofac.Module
    {
        private readonly IConfiguration _configuration;
        public SqlModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            var assembly = typeof(SqlModule)
                .GetTypeInfo()
                .Assembly;

            builder.RegisterAssemblyTypes(assembly)
                .Where(x => x.IsAssignableTo<ISqlRepository>())
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<DatabaseConnectionService>()
              .As<IDatabaseConnectionService>()
              .SingleInstance()
              .WithParameter("connectionString",_configuration.GetSettings<SqlSettings>().ConnectionString);           
        }
    }
}