﻿
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Sonar Code Smell", "S1481:Unused local variables should be removed", Justification = "<Pending>", Scope = "member", Target = "~M:OverhaulMe.Infrastructure.Services.DataInitializer.SeedAsync~System.Threading.Tasks.Task")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Compiler", "CS0219:Variable is assigned but its value is never used", Justification = "<Pending>", Scope = "member", Target = "~M:OverhaulMe.Infrastructure.Services.DataInitializer.SeedAsync~System.Threading.Tasks.Task")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Sonar Code Smell", "S1481:Unused local variables should be removed", Justification = "<Pending>", Scope = "member", Target = "~M:OverhaulMe.Infrastructure.Services.RoomDeviceService.UpdateDeviceAsync(System.Guid,System.Guid)~System.Threading.Tasks.Task")]

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Sonar Code Smell", "S1481:Unused local variables should be removed", Justification = "<Pending>", Scope = "member", Target = "~M:OverhaulMe.Infrastructure.Services.RoomDeviceManager.UpdateDeviceAsync(System.Guid,System.Guid)~System.Threading.Tasks.Task")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Sonar Code Smell", "S1481:Unused local variables should be removed", Justification = "<Pending>", Scope = "member", Target = "~M:OverhaulMe.Infrastructure.Services.RoomDeviceManager.GetDeviceAsync(System.Guid)~System.Threading.Tasks.Task{OverhaulMe.Infrastructure.DTO.DeviceDto}")]

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Sonar Code Smell", "S1481:Unused local variables should be removed", Justification = "<Pending>", Scope = "member", Target = "~M:OverhaulMe.Infrastructure.Services.DuctingProvider.GetByTypeAsync(System.String)~System.Threading.Tasks.Task{OverhaulMe.Infrastructure.DTO.DuctingDto}")]
