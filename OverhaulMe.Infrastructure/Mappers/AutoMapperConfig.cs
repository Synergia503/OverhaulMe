﻿using AutoMapper;
using OverhaulMe.Core.Domain;
using OverhaulMe.Infrastructure.DTO;
using System;
using System.Linq;

namespace OverhaulMe.Infrastructure.Mappers
{
    public static class AutoMapperConfig
    {
        public static IMapper Initialize()
            => new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<User, UserDto>();
                cfg.CreateMap<Room, RoomDto>();

                cfg.CreateMap<Room, RoomDetailsDto>()
                .ForMember(x => x.DevicesPrice, m => m.MapFrom(p => p.Devices.Sum(d => d.Price)))
                .ForMember(x => x.DuctingsPrice, m => m.MapFrom(p => p.Ductings.Sum(d => d.Price)))
                .ForMember(x => x.DuctingsTypes, m => m.MapFrom(p => p.Ductings.Select(d => d.Type)))
                .ForMember(x => x.FurnituresPrice, m => m.MapFrom(p => p.Furnitures.Sum(f => f.Price)))
                .ForMember(x => x.DevicesCount, m => m.MapFrom(p => p.Devices.Count()))
                .ForMember(x => x.NewDevicesCount, m => m.MapFrom(p => p.Devices.Count(d => d.IsNewPurchased)))
                .ForMember(x => x.FurnituresCount, m => m.MapFrom(p => p.Furnitures.Count()))
                .ForMember(x => x.NewFurnituresCount, m => m.MapFrom(p => p.Furnitures.Count(f => f.IsNewPurchased)))
                .ForMember(x => x.DoorsCount, m => m.MapFrom(p => p.Doors.Count()))
                .ForMember(x => x.NewDoorsCount, m => m.MapFrom(p => p.Doors.Count(d => d.IsNewPurchased)))
                .ForMember(x => x.WindowsCount, m => m.MapFrom(p => p.Windows.Count()))
                .ForMember(x => x.NewWindowsCount, m => m.MapFrom(p => p.Windows.Count(w => w.IsNewPurchased)))
                .ForMember(x => x.Brands, m => m.MapFrom(p => p.PaintCans.Select(b => b.BrandName)))
                .ForMember(x => x.PaintSpentVolume, m => m.MapFrom(p => p.PaintCans.Sum(v => (float)Math.Round(v.BoughtVolume - v.RemainingVolume, 2))))
                .ForMember(x => x.PaintBoughtVolume, m => m.MapFrom(p => p.PaintCans.Sum(v => v.BoughtVolume)))
                .ForMember(x => x.PaintRemainingVolume, m => m.MapFrom(p => p.PaintCans.Sum(v => v.RemainingVolume)));

                cfg.CreateMap<Device, DeviceDto>();
                cfg.CreateMap<PaintCan, PaintCanDto>();
                cfg.CreateMap<Ducting, DuctingDto>();
                cfg.CreateMap<Furniture, FurnitureDto>();
                cfg.CreateMap<Window, WindowDto>();
                cfg.CreateMap<Door, DoorDto>()
                .ForMember(x => x.FirstPlace, m => m.MapFrom(p => p.ConnectedPlaces[0]))
                .ForMember(x => x.SecondPlace, m => m.MapFrom(p => p.ConnectedPlaces[1]));
            })
            .CreateMapper();
    }
}