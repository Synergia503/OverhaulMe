﻿using OverhaulMe.Core.Domain;
using OverhaulMe.Core.DomainExceptions;
using OverhaulMe.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.Repositories
{
    public class InMemoryRoomRepository : IRoomRepository
    {
        private static readonly ISet<Room> _rooms = new HashSet<Room>();

        public async Task<IEnumerable<Room>> GetRoomsForUser(Guid userId)
        {
            var roomsForUser = _rooms.Where(r => r.UserId == userId);
            return await Task.FromResult(roomsForUser);
        }

        public async Task<Room> GetSingleRoomForUser(Guid userId, Guid roomId)
        {
            var room = _rooms.SingleOrDefault(r => r.UserId == userId && r.Id == roomId);
            if (room == null)
            {
                throw new RoomException(ErrorCodes.RoomNotExists, $"Room with id: {roomId} does not exist for user with id: {userId}.");
            }

            return await Task.FromResult(room);
        }

        public async Task<Room> GeByIdAsync(Guid roomId)
        {
            var room = _rooms.SingleOrDefault(x => x.Id == roomId);
            return await Task.FromResult(room);
        }

        public async Task<IEnumerable<Room>> BrowseAsync()
            => await Task.FromResult(_rooms);

        public async Task AddAsync(Room room)
        {
            _rooms.Add(room);
            await Task.CompletedTask;
        }

        public async Task DeleteAsync(Room room)
        {
            _rooms.Remove(room);
            await Task.CompletedTask;
        }

        public async Task UpdateAsync(Room room)
        {
            await Task.CompletedTask;
        }
    }
}