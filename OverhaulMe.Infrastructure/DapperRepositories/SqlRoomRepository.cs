﻿using Dapper;
using OverhaulMe.Core.Domain;
using OverhaulMe.Core.Repositories;
using OverhaulMe.Infrastructure.Services.DapperServices;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.DapperRepositories
{
    public class SqlRoomRepository : IRoomRepository, ISqlRepository
    {
        private readonly SqlConnection _sqlConnection;

        public SqlRoomRepository(IDatabaseConnectionService databaseConnection)
        {
            _sqlConnection = databaseConnection.CreateConnection();
        }

        public Task AddAsync(Room room)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Room>> BrowseAsync()
        {
            var rooms = await _sqlConnection.QueryAsync<Room>("GetRooms", new { }, commandType: CommandType.StoredProcedure);

            return rooms;
        }

        public Task DeleteAsync(Room room)
        {
            throw new NotImplementedException();
        }

        public Task<Room> GeByIdAsync(Guid roomId)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Room>> GetRoomsForUser(Guid userId)
        {
            var rooms = await _sqlConnection.QueryAsync<Room>("GetRoomsForUser",
                new { UserId = userId },
                commandType: CommandType.StoredProcedure);

            return rooms;
        }

        public Task<Room> GetSingleRoomForUser(Guid userId, Guid roomId)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(Room room)
        {
            throw new NotImplementedException();
        }
    }
}