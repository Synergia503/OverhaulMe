﻿using Dapper;
using OverhaulMe.Core.Domain;
using OverhaulMe.Core.Repositories;
using OverhaulMe.Infrastructure.Services.DapperServices;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace OverhaulMe.Infrastructure.DapperRepositories
{
    public class SqlUserRepository : IUserRepository, ISqlRepository
    {
        private readonly SqlConnection _sqlConnection;

        public SqlUserRepository(IDatabaseConnectionService databaseConnection)
        {
            _sqlConnection = databaseConnection.CreateConnection();
        }

        public Task AddAsync(User user)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<User>> BrowseAsync()
        {
            var users = await _sqlConnection.QueryAsync<User>("GetUsers", new { }, commandType: CommandType.StoredProcedure);

            return users;
        }

        public async Task DeleteAsync(User user)
        {
            throw new NotImplementedException();
        }

        public async Task<User> GetAsync(Guid id)
        {
            var user = await _sqlConnection.QuerySingleOrDefaultAsync<User>("GetUserById",
                new { Id = id },
                commandType: CommandType.StoredProcedure);
            return user;
        }

        public async Task<User> GetAsync(string email)
        {
            var user = await _sqlConnection.QuerySingleOrDefaultAsync<User>("GetUserByEmail",
                 new { Email = email },
                 commandType: CommandType.StoredProcedure);
            return user;
        }

        public Task UpdateAsync(User user)
        {
            throw new NotImplementedException();
        }
    }
}