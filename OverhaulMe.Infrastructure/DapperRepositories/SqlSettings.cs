﻿namespace OverhaulMe.Infrastructure.DapperRepositories
{
    public class SqlSettings
    {
        public string ConnectionString { get; set; }
        public bool InMemory { get; set; }
    }
}