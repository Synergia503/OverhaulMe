﻿using FluentAssertions;
using Newtonsoft.Json;
using OverhaulMe.Infrastructure.Commands.Ductings;
using OverhaulMe.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace OverhaulMe.Tests.EndToEnd.Controllers
{
    public class DuctingsContollerTests : ControllerTestsBase
    {
        [Fact]
        public async Task browse_async_should_return_all_ductings()
        {
            var response = await Client.GetAsync($"ductings");
            response.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.OK);
            var responseString = await response.Content.ReadAsStringAsync();
            var ductingsDto = JsonConvert.DeserializeObject<IEnumerable<DuctingDto>>(responseString);
            ductingsDto.Should().NotBeNullOrEmpty();
            ductingsDto.Should().AllBeOfType<DuctingDto>();
        }

        [Fact]
        public async Task get_by_type_async_should_return_ductings_with_that_type()
        {
            var type = "TestType1";
            var response = await Client.GetAsync($"ductings/{type}");
            response.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.OK);
            var responseString = await response.Content.ReadAsStringAsync();
            var ductingsDto = JsonConvert.DeserializeObject<IEnumerable<DuctingDto>>(responseString);
            ductingsDto.Should().NotBeNullOrEmpty();
            ductingsDto.Should().AllBeOfType<DuctingDto>();
        }

        [Fact]
        public async Task given_correct_data_ducting_should_be_created()
        {
            var guidForTest = Guid.Parse("11111111-aaaa-1111-1111-111111aaaaaa");
            var command = new CreateDucting() { RoomId = guidForTest, Type = "TestType100", Length = 10f, Place = "TestRoom1", Price = 5m, BoughtPlace = "TestShop1", PurchasedAt = DateTime.UtcNow, AssembledAt = DateTime.UtcNow.AddDays(20) };
            var payload = GetPayload(command);
            var response = await Client.PostAsync($"ductings", payload);
            response.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.NoContent);
            var room = await GetRoomAsync(guidForTest);

            var ductingsDto = await GetDuctingsAsync(guidForTest);
            room.Ductings.Should().NotBeNullOrEmpty();
            var ducting = room.Ductings.SingleOrDefault(d => d.Type == command.Type);

            ductingsDto.Should().NotBeNullOrEmpty();
            ductingsDto.Should().AllBeOfType<DuctingDto>();
            ducting.Should().NotBeNull();
            ductingsDto.Should().Contain(d => d.Place == command.Place);
            room.Name.ShouldBeEquivalentTo(ducting.Place);
        }

        [Fact]
        public async Task browse_anync_for_room_when_ducting_exists_should_return_ductings_list()
        {
            var guidForTest = Guid.Parse("11111111-aaaa-1111-1111-111111aaaaaa");
            var response = await Client.GetAsync($"ductings/rooms/{guidForTest}");
            var responseString = await response.Content.ReadAsStringAsync();
            var ductingsDto = JsonConvert.DeserializeObject<IEnumerable<DuctingDto>>(responseString);
            response.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.OK);
            ductingsDto.Should().NotBeNullOrEmpty();
            ductingsDto.Should().Contain(d => d.RoomId == guidForTest);
            ductingsDto.Should().AllBeOfType<DuctingDto>();
        }

        [Fact]
        public async Task delete_request_should_delete_ducting()
        {
            var guidForTest = Guid.Parse("11111111-aaaa-1111-1111-111111aaaaaa");
            var type = "TestType1";
            var command = new DeleteDucting() { RoomId = guidForTest, Type = type };

            var room = await GetRoomAsync(command.RoomId);
            var ducting = room.Ductings.SingleOrDefault(d => d.Type == command.Type);

            var responseFromDelete = await Client.DeleteAsync($"ductings/{guidForTest}/{type}");
            responseFromDelete.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.NoContent);
            room = await GetRoomAsync(command.RoomId);
            room.Ductings.Should().NotBeNullOrEmpty();
            room.Ductings.Should().NotContain(ducting);
        }

        private async Task<IEnumerable<DuctingDto>> GetDuctingsAsync(Guid roomId)
        {
            var response = await Client.GetAsync($"ductings/rooms/{roomId}");
            var responseString = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<IEnumerable<DuctingDto>>(responseString);
        }

        private async Task<RoomDetailsDto> GetRoomAsync(Guid roomId)
        {
            var response = await Client.GetAsync($"rooms/{roomId}");
            var responseString = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<RoomDetailsDto>(responseString);
        }
    }
}