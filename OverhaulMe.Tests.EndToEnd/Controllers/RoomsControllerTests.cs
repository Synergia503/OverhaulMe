﻿using FluentAssertions;
using Newtonsoft.Json;
using OverhaulMe.Core.DomainExceptions;
using OverhaulMe.Infrastructure.Commands.Rooms;
using OverhaulMe.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace OverhaulMe.Tests.EndToEnd.Controllers
{
    public class RoomsControllerTests : ControllerTestsBase
    {
        [Fact]
        public async Task browse_async_should_return_all_rooms()
        {
            var response = await Client.GetAsync($"rooms");
            response.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.OK);
            var responseString = await response.Content.ReadAsStringAsync();
            var roomsDto = JsonConvert.DeserializeObject<IEnumerable<RoomDto>>(responseString);
            roomsDto.Should().NotBeNullOrEmpty();
            roomsDto.Should().AllBeOfType<RoomDto>();
        }

        [Fact]
        public async Task given_valid_data_room_should_be_created()
        {
            var command = new CreateRoom() { CeilingSize = 9.99f, FloorSize = 10f, WallSize = 15f, RoomName = "Room101" };
            var payload = GetPayload(command);
            var response = await Client.PostAsync($"rooms", payload);

            response.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.Created);
            response.Headers.Location.ToString().ToLowerInvariant().ShouldBeEquivalentTo($"rooms/{command.RoomName.ToLowerInvariant()}");

            var room = await GetRoomAsync(command.RoomName);
            room.Name.ShouldBeEquivalentTo(command.RoomName);
        }

        [Fact]
        public async Task given_valid_id_room_should_exist()
        {
            var guidForTest = Guid.Parse("11111111-aaaa-1111-1111-111111aaaaaa");
            var roomName = "TestRoom1";
            var room = await GetRoomAsync(guidForTest);
            room.Should().NotBeNull();
            room.Name.ShouldBeEquivalentTo(roomName);
        }

        [Fact]
        public async Task given_invalid_id_room_should_not_exist()
        {
            var guidForTest = Guid.Parse("11111111-bbbb-1111-1111-111111aaaaaa");
            var asyncObject = GetRoomAsync(guidForTest);

            Func<Task> act = async () => { await asyncObject; };
            var ex = act.ShouldThrowExactly<RoomException>();
            ex.And.Message.Should().Be("room_not_exists");
        }

        [Fact]
        public async Task given_valid_name_room_should_exist()
        {
            var guidForTest = Guid.Parse("11111111-aaaa-1111-1111-111111aaaaaa");
            var roomName = "TestRoom1";
            var room = await GetRoomAsync(roomName);
            room.Should().NotBeNull();
            room.Id.ShouldBeEquivalentTo(guidForTest);
        }

        [Fact]
        public async Task delete_request_should_delete_room()
        {
            var command = new CreateRoom() { CeilingSize = 9.99f, FloorSize = 10f, WallSize = 15f, RoomName = "Room10001" };
            var payload = GetPayload(command);
            await Client.PostAsync($"rooms", payload);
            var room = await GetRoomAsync(command.RoomName);

            room.Name.ShouldBeEquivalentTo(command.RoomName);

            var responseFromDelete = await Client.DeleteAsync($"rooms/{room.Id}");
            responseFromDelete.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.NoContent);
            var asyncObject = GetRoomAsync(room.Id);
            Func<Task> requestAsync = async () => { await asyncObject; };
            var ex = requestAsync.ShouldThrowExactly<RoomException>();
            ex.And.Message.Should().Be("room_not_exists");
        }

        private async Task<RoomDetailsDto> GetRoomAsync(string name)
        {
            var response = await Client.GetAsync($"rooms/{name}");
            var responseString = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<RoomDetailsDto>(responseString);
        }

        private async Task<RoomDetailsDto> GetRoomAsync(Guid roomId)
        {
            var response = await Client.GetAsync($"rooms/{roomId}");
            var responseString = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<RoomDetailsDto>(responseString);
        }
    }
}