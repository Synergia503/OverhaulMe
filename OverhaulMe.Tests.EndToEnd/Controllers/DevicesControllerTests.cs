﻿using FluentAssertions;
using Newtonsoft.Json;
using OverhaulMe.Infrastructure.Commands.Devices;
using OverhaulMe.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace OverhaulMe.Tests.EndToEnd.Controllers
{
    public class DevicesControllerTests : ControllerTestsBase
    {
        //TODO: Add everywhere where it is needed "only unique items"

        [Fact]
        public async Task given_valid_data_device_should_be_created()
        {
            var guidForTest = Guid.Parse("11111111-aaaa-1111-1111-111111aaaaaa");
            var command = new CreateDevice() { DeviceName = "TestDevice1", DevicePrice = 12m, IsNewPurchased = true, PurchasedAt = DateTime.UtcNow, RoomId = guidForTest };
            var payload = GetPayload(command);
            var response = await Client.PostAsync($"devices", payload);
            response.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.NoContent);
            var room = await GetRoomAsync(command.RoomId);
            var device = room.Devices.FirstOrDefault();

            var deviceDto = await GetDeviceAsync(device.DeviceId);

            device.Should().NotBeNull();
            deviceDto.Should().NotBeNull();
            device.ShouldBeEquivalentTo(deviceDto);

            room.Name.ShouldBeEquivalentTo(device.Place);
        }

        [Fact]
        public async Task browse_anync_when_device_exist_should_return_devices_list()
        {
            var roomName = "TestRoom1";
            var response = await Client.GetAsync($"devices");
            var responseString = await response.Content.ReadAsStringAsync();
            var devicesDto = JsonConvert.DeserializeObject<IEnumerable<DeviceDto>>(responseString);
            response.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.OK);
            devicesDto.Should().NotBeNullOrEmpty();
            devicesDto.Should().Contain(x => x.Place == roomName);
            devicesDto.Should().AllBeOfType<DeviceDto>();
        }

        [Fact]
        public async Task browse_async_for_room_should_return_room_details_dto()
        {
            var guidForTest = Guid.Parse("11111111-aaaa-1111-1111-111111aaaaaa");
            var roomName = "TestRoom1";

            var response = await Client.GetAsync($"devices/rooms/{guidForTest}");
            var responseString = await response.Content.ReadAsStringAsync();
            var devicesDto = JsonConvert.DeserializeObject<IEnumerable<DeviceDto>>(responseString);
            response.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.OK);
            devicesDto.Should().NotBeNullOrEmpty();
            devicesDto.Should().Contain(x => x.Place == roomName);
            devicesDto.Should().AllBeOfType<DeviceDto>();
        }

        [Fact]
        public async Task get_device_async_by_id_should_return_devices_in_room()
        {
            var roomName = "TestRoom1";
            var deviceIdForTest1 = Guid.Parse("11111111-bbbb-1111-1111-111111aaaaaa");
            var response = await Client.GetAsync($"devices/{deviceIdForTest1}");
            var responseString = await response.Content.ReadAsStringAsync();
            var deviceDto = JsonConvert.DeserializeObject<DeviceDto>(responseString);
            response.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.OK);
            deviceDto.Should().NotBeNull();
            deviceDto.Place.ShouldBeEquivalentTo(roomName);
        }

        [Fact]
        public async Task delete_request_should_delete_device()
        {
            var guidForTest = Guid.Parse("11111111-aaaa-1111-1111-111111aaaaaa");
            var command = new CreateDevice() { DeviceName = "TestDevice1", DevicePrice = 12m, IsNewPurchased = true, PurchasedAt = DateTime.UtcNow, RoomId = guidForTest };
            var payload = GetPayload(command);
            var response = await Client.PostAsync($"devices", payload);
            response.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.NoContent);

            var room = await GetRoomAsync(command.RoomId);
            room.Devices.Should().NotBeNullOrEmpty();
            var device = room.Devices.FirstOrDefault();

            var responseFromDelete = await Client.DeleteAsync($"devices/{device.DeviceId}");
            responseFromDelete.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.NoContent);
            room = await GetRoomAsync(command.RoomId);
            room.Devices.Should().NotBeNullOrEmpty();
            room.Devices.Should().NotContain(device);
        }

        private async Task<DeviceDto> GetDeviceAsync(Guid deviceId)
        {
            var response = await Client.GetAsync($"devices/{deviceId}");
            var responseString = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<DeviceDto>(responseString);
        }

        private async Task<RoomDetailsDto> GetRoomAsync(string name)
        {
            var response = await Client.GetAsync($"rooms/{name}");
            var responseString = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<RoomDetailsDto>(responseString);
        }

        private async Task<RoomDetailsDto> GetRoomAsync(Guid roomId)
        {
            var response = await Client.GetAsync($"rooms/{roomId}");
            var responseString = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<RoomDetailsDto>(responseString);
        }
    }
}