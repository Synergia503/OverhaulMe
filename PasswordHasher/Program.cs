﻿using Autofac;
using Microsoft.Extensions.Configuration;
using PasswordHasher.Application;
using PasswordHasher.IoC;
using System;
using System.IO;
using System.Threading.Tasks;

namespace PasswordHasher
{
    class Program
    {
        public static async Task Main(string[] args)
        {
            Console.WriteLine("Program started...");
            IConfiguration configuration = GetConfig();
            IContainer container = RegisterAll(configuration);
            using (ILifetimeScope scope = container.BeginLifetimeScope())
            {
                IApplication app = scope.Resolve<IApplication>();
                var usersToUpdate = await app.RunHashing();
                await app.UpdateUsersAsync(usersToUpdate);
            }

            Console.WriteLine("Program finished...");
            Console.Read();
        }

        private static IConfiguration GetConfig()
        {
            IConfiguration config = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", true, true)
                .Build();

            return config;
        }

        private static IContainer RegisterAll(IConfiguration configuration)
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<Application.Application>().As<IApplication>();
            builder.RegisterModule(new ContainerModule(configuration));
            return builder.Build();
        }
    }
}