﻿using PasswordHasher.Domain;
using PasswordHasher.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PasswordHasher.Application
{
    public class Application : IApplication
    {
        private readonly IUserService _userService;

        public Application(IUserService userService)
        {
            _userService = userService;
        }

        public async Task<IEnumerable<User>> RunHashing()
        {
            Console.WriteLine("Getting users started...");
            IEnumerable<User> users = await _userService.GetAll();
            Console.WriteLine("Getting users finished...");

            Console.WriteLine("Hashing users' passwords started...");
            IEnumerable<User> usersAfterHasing = await _userService.HashPasswordsForUsers(users);
            Console.WriteLine("Hashing users' passwords finished...");

            return usersAfterHasing;
        }

        public async Task UpdateUsersAsync(IEnumerable<User> usersWithHashedPasswords)
        {
            Console.WriteLine("Updating users started...");
            await _userService.UpdateUsers(usersWithHashedPasswords);
            Console.WriteLine("Updating users finished...");
        }
    }
}