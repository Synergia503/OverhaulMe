﻿using PasswordHasher.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PasswordHasher.Application
{
    public interface IApplication
    {
        Task<IEnumerable<User>> RunHashing();
        Task UpdateUsersAsync(IEnumerable<User> usersWithHashedPasswords);
    }
}