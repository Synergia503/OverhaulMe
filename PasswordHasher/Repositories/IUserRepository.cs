﻿using PasswordHasher.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PasswordHasher.Repositories
{
    public interface IUserRepository : IRepository
    {
        Task<IEnumerable<User>> GetAllAsync();
        Task UpdateAllAsync(IEnumerable<User> usersToUpdate);
    }
}