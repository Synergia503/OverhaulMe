﻿using Dapper;
using Dapper.Contrib.Extensions;
using PasswordHasher.Domain;
using PasswordHasher.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace PasswordHasher.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly SqlConnection _sqlConnection;

        public UserRepository(IDatabaseConnectionService databaseConnection)
        {
            _sqlConnection = databaseConnection.CreateConnection();
        }

        public async Task<IEnumerable<User>> GetAllAsync()
        {
            var users = await _sqlConnection.QueryAsync<User>("GetUsers", new { }, commandType: CommandType.StoredProcedure);
            return users;
        }

        public async Task UpdateAllAsync(IEnumerable<User> usersToUpdate)
        {
            try
            {
                await _sqlConnection.UpdateAsync(usersToUpdate);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}