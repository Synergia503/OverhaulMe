﻿using Autofac;
using Microsoft.Extensions.Configuration;

namespace PasswordHasher.IoC
{
    public class ContainerModule : Autofac.Module
    {
        private readonly IConfiguration _configuration;

        public ContainerModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<RepositoryModule>();
            builder.RegisterModule<ServiceModule>();
            builder.RegisterModule(new SqlModule(_configuration.GetSection("sql")["connectionString"]));
        }
    }
}