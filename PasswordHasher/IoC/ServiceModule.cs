﻿using Autofac;
using PasswordHasher.Services;
using System.Reflection;

namespace PasswordHasher.IoC
{
    public class ServiceModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var assembly = typeof(ServiceModule)
                .GetTypeInfo()
                .Assembly;

            builder.RegisterAssemblyTypes(assembly)
               .Where(x => x.IsAssignableTo<IService>())
               .AsImplementedInterfaces()
               .InstancePerLifetimeScope();

            builder.RegisterType<Encrypter>()
              .As<IEncrypter>()
              .SingleInstance();
        }
    }
}