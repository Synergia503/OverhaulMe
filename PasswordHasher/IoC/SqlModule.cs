﻿using Autofac;
using PasswordHasher.Services;
using System.Reflection;

namespace PasswordHasher.IoC
{
    public class SqlModule : Autofac.Module
    {
        private readonly string _connectionString;

        public SqlModule(string connectionString)
        {
            _connectionString = connectionString;
        }

        protected override void Load(ContainerBuilder builder)
        {
            var assembly = typeof(SqlModule)
                .GetTypeInfo()
                .Assembly;            

            builder.RegisterType<DatabaseConnectionService>()
              .As<IDatabaseConnectionService>()
              .SingleInstance()
              .WithParameter("connectionString", _connectionString);
        }
    }
}