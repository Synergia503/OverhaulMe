﻿using Dapper.Contrib.Extensions;
using System;

namespace PasswordHasher.Domain
{
    [Table("[User]")]
    public class User
    {
        public User()
        {

        }

        [ExplicitKey]
        public Guid Id { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
    }
}