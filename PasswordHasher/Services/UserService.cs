﻿using PasswordHasher.Domain;
using PasswordHasher.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PasswordHasher.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IEncrypter _encrypter;

        public UserService(IUserRepository userRepository, IEncrypter encrypter)
        {
            _userRepository = userRepository;
            _encrypter = encrypter;
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            return await _userRepository.GetAllAsync();
        }

        public Task<IEnumerable<User>> HashPasswordsForUsers(IEnumerable<User> users)
        {
            foreach (User user in users)
            {
                var salt = _encrypter.GetSalt();
                var hashedPassword = _encrypter.GetHash(user.Password, salt);
                user.Salt = salt;
                user.Password = hashedPassword;
            }

            return Task.FromResult(users);
        }

        public async Task UpdateUsers(IEnumerable<User> usersToUpdate)
        {
            await _userRepository.UpdateAllAsync(usersToUpdate);
        }
    }
}