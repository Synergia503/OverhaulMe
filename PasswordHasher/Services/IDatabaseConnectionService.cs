﻿using System;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace PasswordHasher.Services
{
    public interface IDatabaseConnectionService : IDisposable
    {
        Task<SqlConnection> CreateConnectionAsync();
        SqlConnection CreateConnection();
    }
}