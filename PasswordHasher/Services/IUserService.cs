﻿using PasswordHasher.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PasswordHasher.Services
{
    public interface IUserService : IService
    {
        Task<IEnumerable<User>> GetAll();
        Task<IEnumerable<User>> HashPasswordsForUsers(IEnumerable<User> users);
        Task UpdateUsers(IEnumerable<User> usersToUpdate);
    }
}