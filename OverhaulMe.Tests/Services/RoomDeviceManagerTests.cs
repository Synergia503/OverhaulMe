﻿using AutoMapper;
using FluentAssertions;
using Moq;
using OverhaulMe.Core.Domain;
using OverhaulMe.Core.Repositories;
using OverhaulMe.Infrastructure.DTO;
using OverhaulMe.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace OverhaulMe.Tests.Services
{
    public class RoomDeviceManagerTests
    {
        [Fact]
        public async Task browse_async_should_return_collection_only_of_device_base_dto()
        {
            var roomId = Guid.NewGuid();
            var room2Id = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var name = "room111";
            var deviceGuid1 = Guid.Parse("11111111-aabb-1111-1111-111111aaaaaa");
            var deviceGuid2 = Guid.Parse("11111111-bbbb-1111-1111-111111aaaaaa");
            var deviceGuid3 = Guid.Parse("11111111-cccc-1111-1111-111111aaaaaa");
            var deviceGuid4 = Guid.Parse("11111111-dddd-1111-1111-111111aaaaaa");
            var room1 = Room.Create(userId, roomId, name, 10f, 20f, 30f);
            var room2 = Room.Create(userId, room2Id, name + "1", 100f, 200f, 300f);

            var deviceDto = new DeviceDto() { DeviceId = deviceGuid1, Place = room1.Name, DeviceName = "device11", Price = 11m, IsNewPurchased = true, PurchasedAt = null };
            var deviceDto2 = new DeviceDto() { DeviceId = deviceGuid2, Place = room1.Name, DeviceName = "device22", Price = 22m, IsNewPurchased = false, PurchasedAt = null };
            IEnumerable<DeviceDto> devicesDtoList = new List<DeviceDto>()
            {
                deviceDto,
                deviceDto2
            };

            var roomDto = new RoomDetailsDto()
            {
                Id = room1.Id,
                Name = room1.Name,
                CeilingSize = room1.CeilingSize,
                FloorSize = room1.FloorSize,
                WallSize = room1.WallSize,
                Devices = new List<DeviceDto>() { deviceDto }
            };

            IEnumerable<Room> roomsList = new List<Room>
            {
                room1,
                room2
            };

            IEnumerable<RoomDto> roomsDtoList = new List<RoomDto>
            {
                new RoomDto()
                {
                    Id = room1.Id,
                    Name = room1.Name,
                    CeilingSize = room1.CeilingSize,
                    FloorSize = room1.FloorSize,
                    WallSize = room1.WallSize
                }
            };

            var roomRepositoryMock = new Mock<IRoomRepository>();
            var userRepositoryMock = new Mock<IUserRepository>();
            var mapperMock = new Mock<IMapper>();
            var roomDeviceManager = new RoomDeviceManager(roomRepositoryMock.Object, userRepositoryMock.Object, mapperMock.Object);
            var roomService = new RoomService(roomRepositoryMock.Object, userRepositoryMock.Object, mapperMock.Object);

            roomRepositoryMock.Setup(x => x.GeByIdAsync(room1.Id)).ReturnsAsync(room1);
            roomRepositoryMock.Setup(x => x.GeByIdAsync(room2.Id)).ReturnsAsync(room2);
            roomRepositoryMock.Setup(x => x.BrowseAsync()).Returns(Task.FromResult(roomsList));
            mapperMock.Setup(x => x.Map<IEnumerable<RoomDto>>(It.IsAny<IEnumerable<Room>>())).Returns(roomsDtoList);
            mapperMock.Setup(x => x.Map<IEnumerable<DeviceDto>>(It.IsAny<IEnumerable<Device>>())).Returns(devicesDtoList);

            await roomDeviceManager.CreateDeviceAsync(userId, roomId, deviceGuid1, "device11", 11m, true, null);
            await roomDeviceManager.CreateDeviceAsync(userId, room2Id, deviceGuid2, "device22", 22m, true, null);
            await roomDeviceManager.CreateDeviceAsync(userId, roomId, deviceGuid3, "device12", 12m, true, null);
            await roomDeviceManager.CreateDeviceAsync(userId, room2Id, deviceGuid4, "device22", 22m, true, null);

            roomRepositoryMock.Verify(x => x.GeByIdAsync(room1.Id), Times.Exactly(2));
            roomRepositoryMock.Verify(x => x.GeByIdAsync(room2.Id), Times.Exactly(2));

            var devicesDto = await roomDeviceManager.BrowseAsync(userId);
            devicesDto.Should().NotBeNullOrEmpty();
            devicesDto.Should().AllBeOfType<DeviceDto>();
            devicesDto.Should().OnlyHaveUniqueItems();
        }

        [Fact]
        public async Task get_device_async_by_id_should_return_device_dto()
        {
            var userId = Guid.NewGuid();
            var guidForTest = Guid.Parse("11111111-aaaa-1111-1111-111111aaaaaa");
            var deviceGuid1 = Guid.Parse("11111111-bbbb-1111-1111-111111aaaaaa");
            var deviceGuid2 = Guid.Parse("11111111-bbcc-1111-1111-111111aaaaaa");
            var deviceGuid3 = Guid.Parse("11111111-cccc-1111-1111-111111aaaaaa");
            var roomName = "TestRoom1";
            var room1 = Room.Create(userId, guidForTest, roomName, 10f, 20f, 30f);
            var deviceDto = new DeviceDto() { DeviceId = deviceGuid1, Place = room1.Name, DeviceName = "device11", Price = 11m, IsNewPurchased = true, PurchasedAt = null };

            var roomDto = new RoomDetailsDto()
            {
                Id = room1.Id,
                Name = room1.Name,
                CeilingSize = room1.CeilingSize,
                FloorSize = room1.FloorSize,
                WallSize = room1.WallSize,
                Devices = new List<DeviceDto>() { deviceDto }
            };
            IEnumerable<Room> roomsList = new List<Room>
            {
                room1
            };

            IEnumerable<RoomDto> roomsDtoList = new List<RoomDto>
            {
                new RoomDto()
                {
                    Id = room1.Id,
                    Name = room1.Name,
                    CeilingSize = room1.CeilingSize,
                    FloorSize = room1.FloorSize,
                    WallSize = room1.WallSize
                }
            };

            var roomRepositoryMock = new Mock<IRoomRepository>();
            var userRepositoryMock = new Mock<IUserRepository>();
            var mapperMock = new Mock<IMapper>();

            var roomDeviceManager = new RoomDeviceManager(roomRepositoryMock.Object, userRepositoryMock.Object, mapperMock.Object);
            mapperMock.Setup(x => x.Map<RoomDetailsDto>(It.IsAny<Room>())).Returns(roomDto);
            mapperMock.Setup(x => x.Map<DeviceDto>(It.IsAny<Device>())).Returns(deviceDto);
            mapperMock.Setup(x => x.Map<IEnumerable<RoomDto>>(It.IsAny<IEnumerable<Room>>())).Returns(roomsDtoList);
            roomRepositoryMock.Setup(x => x.GeByIdAsync(room1.Id)).ReturnsAsync(room1);

            roomRepositoryMock.Setup(x => x.BrowseAsync()).Returns(Task.FromResult(roomsList));


            await roomDeviceManager.CreateDeviceAsync(userId, guidForTest, deviceGuid1, "device11", 11m, true, null);
            await roomDeviceManager.CreateDeviceAsync(userId, guidForTest, deviceGuid2, "device12", 22m, true, null);
            await roomDeviceManager.CreateDeviceAsync(userId, guidForTest, deviceGuid3, "device13", 13m, true, null);

            var device = await roomDeviceManager.GetDeviceAsync(userId, deviceGuid1);
            device.Should().NotBeNull();
            device.Place.ShouldBeEquivalentTo(roomName);
        }
    }
}