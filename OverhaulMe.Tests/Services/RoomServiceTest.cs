﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using Moq;
using Xunit;
using System.Threading.Tasks;
using OverhaulMe.Core.Repositories;
using OverhaulMe.Infrastructure.Services;
using OverhaulMe.Core.Domain;
using OverhaulMe.Infrastructure.DTO;
using AutoMapper;

namespace OverhaulMe.Tests.Services
{
    public class RoomServiceTest
    {
        [Fact]
        public async Task creating_async_should_invoke_add_async_on_repository_only_once()
        {
            var userRepositoryMock = new Mock<IUserRepository>();
            var roomRepositoryMock = new Mock<IRoomRepository>();
            var mapperMock = new Mock<IMapper>();
            var roomService = new RoomService(roomRepositoryMock.Object, userRepositoryMock.Object, mapperMock.Object);
            await roomService.CreateRoomAsync(Guid.NewGuid(), Guid.NewGuid(), "room111", 1f, 2f, 4f);
            roomRepositoryMock.Verify(x => x.AddAsync(It.IsAny<Room>()), Times.Once);
        }

        [Fact]
        public async Task creating_async_should_create_new_room()
        {
            var roomId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var name = "room111";
            var room = Room.Create(userId, roomId, name, 10f, 20f, 30f);
            var roomRepositoryMock = new Mock<IRoomRepository>();
            var userRepositoryMock = new Mock<IUserRepository>();
            var mapperMock = new Mock<IMapper>();
            var roomService = new RoomService(roomRepositoryMock.Object, userRepositoryMock.Object, mapperMock.Object);
            var roomDto = new RoomDetailsDto()
            {
                Id = room.Id,
                CeilingSize = room.CeilingSize,
                FloorSize = room.FloorSize,
                Name = room.Name,
                WallSize = room.WallSize
            };


            roomRepositoryMock.Setup(x => x.GeByIdAsync(roomId)).ReturnsAsync(room);
            mapperMock.Setup(x => x.Map<RoomDetailsDto>(room)).Returns(roomDto);
            var roomDetailsDto = await roomService.GetAsync(userId, room.Id);
            roomDetailsDto.Should().NotBeNull();

            roomDetailsDto.Id.ShouldBeEquivalentTo(roomId);
        }

        [Fact]
        public async Task when_invoking_get_async_it_should_invoke_get_async_once_on_room_repository()
        {
            var userId = Guid.NewGuid();
            var room = Room.Create(userId, Guid.NewGuid(), "room111", 10f, 20f, 30f);
            var roomRepositoryMock = new Mock<IRoomRepository>();
            var userRepositoryMock = new Mock<IUserRepository>();
            var mapperMock = new Mock<IMapper>();
            var roomDto = new RoomDetailsDto()
            {
                Id = room.Id,
                CeilingSize = room.CeilingSize,
                FloorSize = room.FloorSize,
                Name = room.Name,
                WallSize = room.WallSize
            };
            var roomService = new RoomService(roomRepositoryMock.Object, userRepositoryMock.Object, mapperMock.Object);
            roomRepositoryMock.Setup(x => x.GeByIdAsync(room.Id)).ReturnsAsync(room);
            mapperMock.Setup(x => x.Map<RoomDetailsDto>(room)).Returns(roomDto);
            await roomService.GetAsync(userId, room.Id);
            roomRepositoryMock.Verify(x => x.GeByIdAsync(room.Id), Times.Once);
        }

        [Fact]
        public async Task when_invoking_add_device_it_should_add_new_device_to_the_collection()
        {
            var roomId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var name = "room111";
            var room = Room.Create(userId, roomId, name, 10f, 20f, 30f);
            var roomDto = new RoomDetailsDto()
            {
                Id = room.Id,
                CeilingSize = room.CeilingSize,
                FloorSize = room.FloorSize,
                Name = room.Name,
                WallSize = room.WallSize,
                Devices = new List<DeviceDto>() { new DeviceDto() { DeviceName = "device1" } }
            };

            var roomRepositoryMock = new Mock<IRoomRepository>();
            var userRepositoryMock = new Mock<IUserRepository>();
            var mapperMock = new Mock<IMapper>();
            mapperMock.Setup(x => x.Map<RoomDetailsDto>(room)).Returns(roomDto);
            roomRepositoryMock.Setup(x => x.GeByIdAsync(room.Id)).ReturnsAsync(room);

            var roomService = new RoomService(roomRepositoryMock.Object, userRepositoryMock.Object, mapperMock.Object);
            var roomDeviceManager = new RoomDeviceManager(roomRepositoryMock.Object, userRepositoryMock.Object, mapperMock.Object);
            var roomDetailsDto = await roomService.GetAsync(userId, room.Id);

            await roomDeviceManager.CreateDeviceAsync(userId, roomId, Guid.NewGuid(), "device1", 999m, true, null);
            roomDetailsDto.Devices.Should().NotBeNullOrEmpty();
            roomDetailsDto.Devices.Should().Contain(x => x.DeviceName == "device1");
        }

        [Fact]
        public async Task browsing_async_should_return_collection_only_of_room_dto()
        {
            var userId = Guid.NewGuid();
            var roomId = Guid.NewGuid();
            var room2Id = Guid.NewGuid();
            var name = "room111";
            var room1 = Room.Create(userId, roomId, name, 10f, 20f, 30f);
            var room2 = Room.Create(userId, room2Id, name + "1", 100f, 200f, 300f);

            IEnumerable<Room> roomsList = new List<Room>
            {
                room1,
                room2
            };

            IEnumerable<RoomDto> roomsDtoList = new List<RoomDto>
            {
                new RoomDto()
                {
                    Id = room1.Id,
                    Name = room1.Name,
                    CeilingSize = room1.CeilingSize,
                    FloorSize = room1.FloorSize,
                    WallSize = room1.WallSize
                },
                new RoomDto()
                {
                    Id = room2.Id,
                    Name = room2.Name,
                    CeilingSize = room2.CeilingSize,
                    FloorSize = room2.FloorSize,
                    WallSize = room2.WallSize
                }
            };

            var roomRepositoryMock = new Mock<IRoomRepository>();
            var userRepositoryMock = new Mock<IUserRepository>();
            var mapperMock = new Mock<IMapper>();
            mapperMock.Setup(x => x.Map<IEnumerable<RoomDto>>(roomsList)).Returns(roomsDtoList);
            roomRepositoryMock.Setup(x => x.BrowseAsync()).Returns(Task.FromResult(roomsList));
            var roomService = new RoomService(roomRepositoryMock.Object, userRepositoryMock.Object, mapperMock.Object);

            var roomsDto = await roomService.BrowseAsync(userId);
            roomsDto.Should().NotBeNullOrEmpty();
            roomsDto.Should().AllBeOfType<RoomDto>();
            roomsDto.ShouldBeEquivalentTo(roomsList);
        }
    }
}