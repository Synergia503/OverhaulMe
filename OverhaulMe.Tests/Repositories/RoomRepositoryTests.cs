﻿using FluentAssertions;
using Moq;
using OverhaulMe.Core.Domain;
using OverhaulMe.Core.Repositories;
using System;
using System.Threading.Tasks;
using Xunit;

namespace OverhaulMe.Tests.Repositories
{
    public class RoomRepositoryTests
    {
        [Fact]
        public async Task when_creating_new_room_it_should_be_added_correctly_to_the_list()
        {
            var userId = Guid.NewGuid();
            var roomId = Guid.NewGuid();
            var room = Room.Create(userId, roomId, "room100", 10f, 20f, 30f);

            var repositoryMock = new Mock<IRoomRepository>();
            repositoryMock.Setup(x => x.AddAsync(room)).Returns(Task.CompletedTask);
            repositoryMock.Setup(x => x.GeByIdAsync(roomId)).ReturnsAsync(room);

            var existingRoom = await repositoryMock.Object.GeByIdAsync(roomId);
            existingRoom.Should().NotBeNull();
            room.ShouldBeEquivalentTo(existingRoom);
        }

        [Fact]
        public async Task invoking_add_async_should_add_only_one_room_to_the_list()
        {
            var userId = Guid.NewGuid();
            var roomId = Guid.NewGuid();
            var room = Room.Create(userId, roomId, "room100", 10f, 20f, 30f);
            var repositoryMock = new Mock<IRoomRepository>();
            repositoryMock.Setup(x => x.AddAsync(room)).Returns(Task.CompletedTask);
            await repositoryMock.Object.AddAsync(room);
            repositoryMock.Verify(x => x.AddAsync(It.IsAny<Room>()), Times.Once);
        }
    }
}