﻿
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Sonar Code Smell", "S1481:Unused local variables should be removed", Justification = "<Pending>", Scope = "member", Target = "~M:OverhaulMe.Tests.Services.RoomServiceTest.adding_async_should_invoke_create_async_on_repository_only_once~System.Threading.Tasks.Task")]

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Sonar Code Smell", "S1481:Unused local variables should be removed", Justification = "<Pending>", Scope = "member", Target = "~M:OverhaulMe.Tests.Repositories.RoomRepositoryTests.when_creating_new_room_it_should_be_added_correctly_to_the_list~System.Threading.Tasks.Task")]

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Sonar Code Smell", "S1481:Unused local variables should be removed", Justification = "<Pending>", Scope = "member", Target = "~M:OverhaulMe.Tests.Services.RoomServiceTest.when_invoking_get_async_it_should_invoke_get_async_on_room_repository~System.Threading.Tasks.Task")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Sonar Code Smell", "S1481:Unused local variables should be removed", Justification = "<Pending>", Scope = "member", Target = "~M:OverhaulMe.Tests.Services.RoomDeviceManagerTests.browse_async_should_return_collection_only_of_device_base_dto~System.Threading.Tasks.Task")]