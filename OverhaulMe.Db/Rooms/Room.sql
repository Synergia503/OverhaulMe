﻿CREATE TABLE [dbo].[Room]
(
    [UserId] UNIQUEIDENTIFIER NOT NULL , 
    [Id] UNIQUEIDENTIFIER NOT NULL, 
    [Name] NVARCHAR(50) NOT NULL, 
    [FloorSize] FLOAT NOT NULL, 
    [WallSize] FLOAT NOT NULL, 
    [CeilingSize] FLOAT NOT NULL, 
    CONSTRAINT [FK_Room_ToUserId] FOREIGN KEY ([UserId]) REFERENCES [User]([Id]), 
    CONSTRAINT [PK_Room] PRIMARY KEY ([Id])
)

GO
