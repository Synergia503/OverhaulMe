﻿CREATE PROCEDURE [dbo].[GetRoomsForUser]
@UserId UNIQUEIDENTIFIER
AS
BEGIN TRANSACTION
    SET NOCOUNT ON;

    SELECT UserId, Id, [Name], FloorSize, WallSize, CeilingSize
    FROM dbo.[Room]
    Where UserId = @UserId
COMMIT