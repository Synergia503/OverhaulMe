﻿CREATE TABLE [dbo].[User](
	[Id] [uniqueidentifier] NOT NULL,
	[Email] [nvarchar](128) NOT NULL,
	[Username] [nvarchar](64) NOT NULL,
	[Fullname] [nvarchar](96) NOT NULL,
	[Password] [nvarchar](256) NOT NULL,
	[Salt] [nvarchar](128) NOT NULL,
	[Role] [nvarchar](32) NOT NULL,
	[CreatedAt] [datetime] NOT NULL,
	[UpdatedAt] [datetime] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO