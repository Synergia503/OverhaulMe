﻿CREATE PROCEDURE [dbo].[GetUsers]
AS
BEGIN
    SET NOCOUNT ON;

    SELECT Id, Email, Username, Fullname, [Password], Salt, [Role], CreatedAt, UpdatedAt
    FROM dbo.[User]
END