﻿CREATE PROCEDURE [dbo].[GetUserById]
    @Id UNIQUEIDENTIFIER
AS
BEGIN TRANSACTION
    SET NOCOUNT ON;

    SELECT Id, Email, Username, Fullname, [Password], Salt, [Role], CreatedAt, UpdatedAt
    FROM dbo.[User]
    WHERE Id = @Id
COMMIT