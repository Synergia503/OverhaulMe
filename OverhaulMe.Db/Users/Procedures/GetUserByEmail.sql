﻿CREATE PROCEDURE [dbo].[GetUserByEmail]
    @Email nvarchar(128)
AS
BEGIN TRANSACTION
    SET NOCOUNT ON;

    SELECT Id, Email, Username, Fullname, [Password], Salt, [Role], CreatedAt, UpdatedAt
    FROM dbo.[User]
    WHERE Email = @Email
COMMIT