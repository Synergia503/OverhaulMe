﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using OverhaulMe.Api.Framework;
using OverhaulMe.Infrastructure.IoC.Modules;
using OverhaulMe.Infrastructure.Services;
using OverhaulMe.Infrastructure.Settings;
using System;
using System.Text;

namespace OverhaulMe.Api
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public IContainer ApplicationContainer { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().AddJsonOptions(x => x.SerializerSettings.Formatting = Formatting.Indented);
            var jwtSettings = new JwtSettings();
            Configuration.GetSection("jwt").Bind(jwtSettings);

            services.AddAuthentication(authenticationOptions =>
            {
                authenticationOptions.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                authenticationOptions.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                authenticationOptions.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, jwtBearerOptions =>
            {
                jwtBearerOptions.RequireHttpsMetadata = false;
                jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidIssuer = jwtSettings.Issuer,
                    ValidateAudience = false,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSettings.Key))
                };
            });
          
            services.AddCors(o =>
            {
                o.AddPolicy("AllowSpecificOrigin", b => b.AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());
            });

            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("AllowSpecificOrigin"));
            });

            services.AddMemoryCache();

            var builder = new ContainerBuilder();
            builder.Populate(services);
            builder.RegisterModule(new ContainerModule(Configuration));
            ApplicationContainer = builder.Build();
            return new AutofacServiceProvider(ApplicationContainer);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IApplicationLifetime appLifetime, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseAuthentication();
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            app.UseCors("AllowSpecificOrigin");
            app.UseDefaultFiles();

            var generalSettings = app.ApplicationServices.GetService<GeneralSettings>();

            if (generalSettings.SeedRandomData)
            {
                IDataInitializer dataInitializer = app.ApplicationServices.GetService<IDataInitializer>();
                dataInitializer.SeedRandomDataAsync();
            }

            if (generalSettings.SeedSpecificData)
            {
                IDataInitializer dataInitializer = app.ApplicationServices.GetService<IDataInitializer>();
                dataInitializer.SeedSpecificDataAsync();
            }

            app.UseCustomExceptionHandler();
            app.UseStaticFiles();
            appLifetime.ApplicationStopped.Register(() => ApplicationContainer.Dispose());
            app.UseMvc();
        }
    }
}