﻿using Microsoft.AspNetCore.Mvc;
using OverhaulMe.Infrastructure.Commands.Ductings;
using OverhaulMe.Infrastructure.Commands.Interfaces;
using OverhaulMe.Infrastructure.Services;
using System;
using System.Threading.Tasks;

namespace OverhaulMe.Api.Controllers
{
    public class DuctingsController : ApiControllerBase
    {
        private readonly IRoomService _roomService;
        private readonly IDuctingProvider _ductingProvider;

        public DuctingsController(ICommandDispatcher commandDispatcher, IDuctingProvider ductingProvider, IRoomService roomService) : base(commandDispatcher)
        {
            _roomService = roomService;
            _ductingProvider = ductingProvider;
        }

        [HttpGet]
        public async Task<IActionResult> BrowseAllAsync()
        {
            var ductings = await _ductingProvider.BrowseAllAsync(UserId);
            if (ductings == null)
            {
                return NoContent();
            }

            return Json(ductings);

        }

        [HttpGet("{type}")]
        public async Task<IActionResult> Get(string type)
        {
            var ductingsByType = await _ductingProvider.GetByTypeAsync(UserId, type);
            if (ductingsByType == null)
            {
                return NoContent();
            }

            return Json(ductingsByType);
        }

        [HttpGet("rooms/{roomId}")]
        public async Task<IActionResult> BrowseForRoom(Guid roomId)
        {
            var ductings = await _ductingProvider.BrowseForRoomAsync(UserId, roomId);
            if (ductings == null)
            {
                return NoContent();
            }

            return Json(ductings);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CreateDucting command)
        {
            await DispatchAsync(command);
            return NoContent();
        }

        [HttpDelete("{roomId}/{type}")]
        public async Task<IActionResult> Delete(Guid roomId, string type)
        {
            await DispatchAsync(new DeleteDucting() { RoomId = roomId, Type = type });
            return NoContent();
        }
    }
}