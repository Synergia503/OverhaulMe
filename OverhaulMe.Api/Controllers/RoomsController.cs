﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using OverhaulMe.Infrastructure.Commands.Interfaces;
using OverhaulMe.Infrastructure.Commands.Rooms;
using OverhaulMe.Infrastructure.Services;
using System;
using System.Threading.Tasks;

namespace OverhaulMe.Api.Controllers
{
    [Authorize]
    [EnableCors("AllowSpecificOrigin")]
    public class RoomsController : ApiControllerBase
    {
        private readonly IRoomService _roomService;

        public RoomsController(ICommandDispatcher commandDispatcher, IRoomService roomService) : base(commandDispatcher)
        {
            _roomService = roomService;
        }

        [HttpGet]
        public async Task<IActionResult> GetRooms()
        {
            var roomsForUser = await _roomService.BrowseAsync(UserId);
            if (roomsForUser == null)
            {
                return NotFound();
            }

            return Json(roomsForUser);
        }

        [HttpGet]
        [Route("{roomId:guid}")]
        public async Task<IActionResult> Get(Guid roomId)
        {
            var room = await _roomService.GetAsync(UserId, roomId);
            if (room == null)
            {
                return NotFound();
            }

            return Json(room);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CreateRoom command)
        {
            await DispatchAsync(command);

            //Location header -> rooms/name
            return Created($"rooms/{command.RoomId}", null);
        }

        [HttpDelete("{roomId:guid}")]
        public async Task<IActionResult> Delete(Guid roomId)
        {
            await DispatchAsync(new DeleteRoom() { RoomId = roomId });
            return NoContent();
        }
    }
}