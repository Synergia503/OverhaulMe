﻿using Microsoft.AspNetCore.Mvc;
using OverhaulMe.Infrastructure.Commands.Furnitures;
using OverhaulMe.Infrastructure.Commands.Interfaces;
using OverhaulMe.Infrastructure.Services;
using System;
using System.Threading.Tasks;

namespace OverhaulMe.Api.Controllers
{
    public class FurnituresController : ApiControllerBase
    {
        private readonly IFurnitureProvider _furnitureProvider;

        public FurnituresController(IFurnitureProvider furnitureProvider, ICommandDispatcher commandDispatcher) : base(commandDispatcher)
        {
            _furnitureProvider = furnitureProvider;
        }

        [HttpGet("{furnitureId:guid}")]
        public async Task<IActionResult> Get(Guid furnitureId)
        {
            var furniture = await _furnitureProvider.GetFurnitureAsync(UserId, furnitureId);
            if (furniture == null)
            {
                return NoContent();
            }

            return Json(furniture);
        }

        [HttpGet("rooms/{roomId:guid}")]
        public async Task<IActionResult> GetFurnituresForRoom(Guid roomId)
        {
            var furnituresForRoom = await _furnitureProvider.BrowseForRoomAsync(UserId, roomId);
            if (furnituresForRoom == null)
            {
                return NoContent();
            }

            return Json(furnituresForRoom);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllFurnitures()
        {
            var furnituresList = await _furnitureProvider.BrowseAsync(UserId);
            if (furnituresList == null)
            {
                return NoContent();
            }

            return Json(furnituresList);
        }

        [HttpPost]
        public async Task<IActionResult> PostFurniture([FromBody]CreateFurniture command)
        {
            await DispatchAsync(command);
            return NoContent();
        }

        [HttpDelete("{roomId:guid}/{furnitureId:guid}")]
        public async Task<IActionResult> DeleteWindow(Guid roomId, Guid furnitureId)
        {
            await DispatchAsync(new DeleteFurniture() { RoomId = roomId, FurnitureId = furnitureId });
            return NoContent();
        }
    }
}