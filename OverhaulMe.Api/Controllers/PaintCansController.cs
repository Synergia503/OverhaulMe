﻿using Microsoft.AspNetCore.Mvc;
using OverhaulMe.Infrastructure.Commands.Interfaces;
using OverhaulMe.Infrastructure.Commands.PaintCans;
using OverhaulMe.Infrastructure.Services;
using System;
using System.Threading.Tasks;

namespace OverhaulMe.Api.Controllers
{
    public class PaintCansController : ApiControllerBase
    {
        private readonly IPaintCanProvider _paintCanProvider;

        public PaintCansController(ICommandDispatcher commandDispatcher, IPaintCanProvider paintCanProvider) : base(commandDispatcher)
        {
            _paintCanProvider = paintCanProvider;
        }

        [HttpGet]
        public async Task<IActionResult> BrowseAllAsync()
        {
            var paintCans = await _paintCanProvider.BrowseAllAsync(UserId);
            if (paintCans == null)
            {
                return NoContent();
            }

            return Json(paintCans);
        }

        [HttpGet("{brandName}")]
        public async Task<IActionResult> Get(string brandName)
        {
            var paintCansByBrandName = await _paintCanProvider.GetByBrandNameAsync(UserId, brandName);
            if (paintCansByBrandName == null)
            {
                return NoContent();
            }

            return Json(paintCansByBrandName);
        }

        [HttpGet("rooms/{roomId}")]
        public async Task<IActionResult> BrowseForRoom(Guid roomId)
        {
            var paintCans = await _paintCanProvider.BrowseForRoomAsync(UserId, roomId);
            if (paintCans == null)
            {
                return NoContent();
            }

            return Json(paintCans);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CreatePaintCan command)
        {
            await DispatchAsync(command);
            return NoContent();
        }

        [HttpDelete("{roomId}/{brandName}/{canNumbersToRemove}")]
        public async Task<IActionResult> Delete(Guid roomId, string brandName, int canNumbersToRemove)
        {
            await DispatchAsync(new DeletePaintCan() { RoomId = roomId, BrandName = brandName, CanNumbersToRemove = canNumbersToRemove });
            return NoContent();
        }
    }
}