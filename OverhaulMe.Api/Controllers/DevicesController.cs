﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using OverhaulMe.Infrastructure.Commands.Devices;
using OverhaulMe.Infrastructure.Commands.Interfaces;
using OverhaulMe.Infrastructure.Services;
using System;
using System.Threading.Tasks;

namespace OverhaulMe.Api.Controllers
{
    [Authorize]
    [EnableCors("AllowSpecificOrigin")]
    public class DevicesController : ApiControllerBase
    {
        private readonly IRoomDeviceManager _roomDeviceManager;
        private readonly IRoomService _roomService;

        public DevicesController(ICommandDispatcher commandDispatcher, IRoomDeviceManager roomDeviceManager, IRoomService roomService) : base(commandDispatcher)
        {
            _roomDeviceManager = roomDeviceManager;
            _roomService = roomService;
        }

        [HttpGet("{deviceId:guid}")]
        public async Task<IActionResult> Get(Guid deviceId)
        {
            var device = await _roomDeviceManager.GetDeviceAsync(UserId, deviceId);
            if (device == null)
            {
                return NoContent();
            }

            return Json(device);
        }

        [HttpGet("rooms/{roomId:guid}")]
        public async Task<IActionResult> GetDevicesForRoom(Guid roomId)
        {
            var devicesForRoom = await _roomDeviceManager.BrowseForRoomAsync(UserId, roomId);
            if (devicesForRoom == null)
            {
                return NoContent();
            }

            return Json(devicesForRoom);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllDevices()
        {
            var devicesList = await _roomDeviceManager.BrowseAsync(UserId);
            if (devicesList == null)
            {
                return NoContent();
            }

            return Json(devicesList);
        }

        [HttpPost]
        public async Task<IActionResult> PostDevice([FromBody]CreateDevice command)
        {
            await DispatchAsync(command);
            return NoContent();
        }

        [HttpDelete("{roomId:guid}/{deviceId:guid}")]
        public async Task<IActionResult> DeleteDevice(Guid roomId, Guid deviceId)
        {
            await DispatchAsync(new DeleteDevice() { RoomId = roomId, DeviceId = deviceId });
            return NoContent();
        }
    }
}