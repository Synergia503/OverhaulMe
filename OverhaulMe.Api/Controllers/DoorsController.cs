﻿using Microsoft.AspNetCore.Mvc;
using OverhaulMe.Infrastructure.Commands.Doors;
using OverhaulMe.Infrastructure.Commands.Interfaces;
using OverhaulMe.Infrastructure.Services;
using System;
using System.Threading.Tasks;

namespace OverhaulMe.Api.Controllers
{
    public class DoorsController : ApiControllerBase
    {
        private readonly IDoorProvider _doorProvider;

        public DoorsController(IDoorProvider doorProvider, ICommandDispatcher commandDispatcher) : base(commandDispatcher)
        {
            _doorProvider = doorProvider;
        }

        [HttpGet]
        public async Task<IActionResult> Get(string firstPlace, string secondPlace)
        {
            var door = await _doorProvider.GetSingleDoorAsync(UserId, firstPlace, secondPlace);
            if (door == null)
            {
                return NoContent();
            }

            return Json(door);
        }

        [HttpGet("rooms/{roomId:guid}")]
        public async Task<IActionResult> GetDoorsForRoomAsync(Guid roomId)
        {
            var doorsForRoom = await _doorProvider.BrowseForRoomAsync(UserId, roomId);
            if (doorsForRoom == null)
            {
                return NoContent();
            }

            return Json(doorsForRoom);
        }

        [HttpGet("all")]
        public async Task<IActionResult> GetAllDoorsAsync()
        {
            var doorsList = await _doorProvider.BrowseAsync(UserId);
            if (doorsList == null)
            {
                return NoContent();
            }

            return Json(doorsList);
        }

        [HttpPost]
        public async Task<IActionResult> PostDoor([FromBody]CreateDoor command)
        {
            await DispatchAsync(command);
            return NoContent();
        }

        [HttpDelete("{roomId:guid}")]
        public async Task<IActionResult> DeleteWindow(Guid roomId, string firstPlace, string secondPlace)
        {
            await DispatchAsync(new DeleteDoor() { RoomId = roomId, FirstPlace = firstPlace, SecondPlace = secondPlace });
            return NoContent();
        }
    }
}