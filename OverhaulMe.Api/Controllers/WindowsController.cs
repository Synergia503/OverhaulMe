﻿using Microsoft.AspNetCore.Mvc;
using OverhaulMe.Infrastructure.Commands.Interfaces;
using OverhaulMe.Infrastructure.Commands.Windows;
using OverhaulMe.Infrastructure.Services;
using System;
using System.Threading.Tasks;

namespace OverhaulMe.Api.Controllers
{
    public class WindowsController : ApiControllerBase
    {
        private readonly IWindowProvider _windowProvider;

        public WindowsController(IWindowProvider windowProvider, ICommandDispatcher commandDispatcher) : base(commandDispatcher)
        {
            _windowProvider = windowProvider;
        }

        [HttpGet("{windowId:guid}")]
        public async Task<IActionResult> Get(Guid windowId)
        {
            var window = await _windowProvider.GetWindowAsync(UserId, windowId);
            if (window == null)
            {
                return NoContent();
            }

            return Json(window);
        }

        [HttpGet("rooms/{roomId:guid}")]
        public async Task<IActionResult> GetWindowsForRoom(Guid roomId)
        {
            var windowsForRoom = await _windowProvider.BrowseForRoomAsync(UserId, roomId);
            if (windowsForRoom == null)
            {
                return NoContent();
            }

            return Json(windowsForRoom);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllWindows()
        {
            var windowsList = await _windowProvider.BrowseAsync(UserId);
            if (windowsList == null)
            {
                return NoContent();
            }

            return Json(windowsList);
        }

        [HttpPost]
        public async Task<IActionResult> PostWindow([FromBody]CreateWindow command)
        {
            await DispatchAsync(command);
            return NoContent();
        }

        [HttpDelete("{roomId:guid}/{windowId:guid}")]
        public async Task<IActionResult> DeleteWindow(Guid roomId, Guid windowId)
        {
            await DispatchAsync(new DeleteWindow() { RoomId = roomId, WindowId = windowId });
            return NoContent();
        }
    }
}