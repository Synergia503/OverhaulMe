﻿using System;

namespace OverhaulMe.Core.DomainExceptions
{
    public class RoomException : Exception, IDomainException
    {
        public RoomException()
        {

        }

        public RoomException(string errorCode) : this(errorCode, null, null, null)
        {

        }

        public RoomException(string message, params object[] args) : this(null, null, message, args)
        {

        }

        protected RoomException(string errorCode, string message, params object[] args)
           : this(errorCode, null, message, args)
        {

        }


        public RoomException(Exception innerException, string message, params object[] args)
            : this(string.Empty, innerException, message, args)
        {

        }

        public RoomException(string errorCode, Exception innerException, string message, params object[] args)
            : base(string.Format(message, args), innerException)
        {
            ErrorCode = errorCode;
        }
        public string ErrorCode { get; }
    }
}