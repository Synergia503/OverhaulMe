﻿using System;

namespace OverhaulMe.Core.DomainExceptions
{
    public class UserException : Exception, IDomainException
    {
        public UserException()
        {

        }

        public UserException(string errorCode) : this(errorCode, null, null, null)
        {

        }

        public UserException(string message, params object[] args) : this(null, null, message, args)
        {

        }

        protected UserException(string errorCode, string message, params object[] args)
           : this(errorCode, null, message, args)
        {

        }


        public UserException(Exception innerException, string message, params object[] args)
            : this(string.Empty, innerException, message, args)
        {

        }

        public UserException(string errorCode, Exception innerException, string message, params object[] args)
            : base(string.Format(message, args), innerException)
        {
            ErrorCode = errorCode;
        }
        public string ErrorCode { get; }
    }
}