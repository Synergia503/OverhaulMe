﻿using System;

namespace OverhaulMe.Core.DomainExceptions
{
    public class DoorException : Exception, IDomainException
    {
        public DoorException()
        {

        }

        public DoorException(string errorCode) : this(errorCode, null, null, null)
        {

        }

        public DoorException(string message, params object[] args) : this(null, null, message, args)
        {

        }

        protected DoorException(string errorCode, string message, params object[] args)
           : this(errorCode, null, message, args)
        {

        }


        public DoorException(Exception innerException, string message, params object[] args)
            : this(string.Empty, innerException, message, args)
        {

        }

        public DoorException(string errorCode, Exception innerException, string message, params object[] args)
            : base(string.Format(message, args), innerException)
        {
            ErrorCode = errorCode;
        }
        public string ErrorCode { get; }
    }
}