﻿namespace OverhaulMe.Core.DomainExceptions
{
    public static class ErrorCodes
    {
        public static string RoomAlreadyExists => "room_already_exists";
        public static string RoomsNotExist => "rooms_not_exist";
        public static string RoomNotExists => "room_not_exists";
        public static string DeviceNotExists => "device_not_exists";
        public static string DevicePriceEqualOrBelowZero => "device_price_equal_or_below_zero";
        public static string EmptyDeviceName => "empty_device_name";
        public static string EmptyPlaceName => "empty_place_name";
        public static string EmptyRoomName => "empty_room_name";
        public static string FloorSizeEqualOrBelowZero => "floor_size_below_zero_or_equal_zero";
        public static string WallSizeEqualOrBelowZero => "wall_size_below_zero_or_equal_zero";
        public static string CeilingSizeEqualOrBelowZero => "ceiling_size_below_zero_or_equal_zero";
        public static string DuctingAlreadyExists => "ducting_already_exists";
        public static string EmptyPaintColour => "empty_paint_color";
        public static string EmptyPaintBrandName => "empty_paint_brand_name";
        public static string EmptyPaintPlace => "empty_painted_place";
        public static string PaintVolumeEqualOrBelowZero => "paint_volume_below_zero_or_equal_zero";
        public static string PaintPriceEqualOrBelowZero => "paint_price_equal_or_below_zero";
        public static string EmptyWindowBrandName => "empty_window_brand_name";
        public static string EmptyWindowPlace => "empty_window_place";
        public static string WindowHeightEqualOrBelowZero => "window_height_below_zero_or_equal_zero";
        public static string WindowWidthEqualOrBelowZero => "window_width_below_zero_or_equal_zero";
        public static string WindowDepthEqualOrBelowZero => "window_depth_below_zero_or_equal_zero";
        public static string WindowPriceEqualOrBelowZero => "window_price_equal_or_below_zero";
        public static string EmptyWindowKind => "empty_window_kind";
        public static string EmptyDoorPlace => "empty_door_place";
        public static string EmptyDoorKind => "empty_door_kind";
        public static string EmptyDoorBrandName => "empty_door_brand_name";
        public static string DoorPriceEqualOrBelowZero => "door_price_below_zero_or_equal_zero";
        public static string DoorDepthEqualOrBelowZero => "door_depth_below_zero_or_equal_zero";
        public static string DoorWidthEqualOrBelowZero => "door_width_below_zero_or_equal_zero";
        public static string DoorHeightEqualOrBelowZero => "door_height_below_zero_or_equal_zero";
        public static string WrongDoorStartWorkDate => "start_door_work_date_later_than_door_end_date";
        public static string WrongDoorEndWorkDate => "end_door_work_date_earlier_than_door_start_date";
        public static string WorkHoursEqualOrBelowZero => "door_work_hours_below_zero_or_equal_zero";
        public static string WindowNotExists => "window_not_exists";
        public static string InvalidEmail => "invalid_email";
        public static string InvalidPassword => "invalid_password";
        public static string InvalidRole => "invalid_role";
        public static string InvalidUserName => "invalid_username";
        public static string InvalidFullName => "invalid_fullname";
        public static string UserNotExists => "user_not_exists";
    }
}