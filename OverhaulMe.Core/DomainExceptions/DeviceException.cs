﻿using System;

namespace OverhaulMe.Core.DomainExceptions
{
    public class DeviceException : Exception, IDomainException
    {
        public DeviceException()
        {

        }

        public DeviceException(string errorCode) : this(errorCode, null, null, null)
        {

        }

        public DeviceException(string message, params object[] args) : this(null, null, message, args)
        {

        }

        protected DeviceException(string errorCode, string message, params object[] args)
           : this(errorCode, null, message, args)
        {

        }


        public DeviceException(Exception innerException, string message, params object[] args)
            : this(string.Empty, innerException, message, args)
        {

        }

        public DeviceException(string errorCode, Exception innerException, string message, params object[] args)
            : base(string.Format(message, args), innerException)
        {
            ErrorCode = errorCode;
        }
        public string ErrorCode { get; }
    }
}