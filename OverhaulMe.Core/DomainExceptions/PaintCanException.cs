﻿using System;

namespace OverhaulMe.Core.DomainExceptions
{
    public class PaintCanException : Exception, IDomainException
    {
        public PaintCanException()
        {

        }

        public PaintCanException(string errorCode) : this(errorCode, null, null, null)
        {

        }

        public PaintCanException(string message, params object[] args) : this(null, null, message, args)
        {

        }

        protected PaintCanException(string errorCode, string message, params object[] args)
           : this(errorCode, null, message, args)
        {

        }


        public PaintCanException(Exception innerException, string message, params object[] args)
            : this(string.Empty, innerException, message, args)
        {

        }

        public PaintCanException(string errorCode, Exception innerException, string message, params object[] args)
            : base(string.Format(message, args), innerException)
        {
            ErrorCode = errorCode;
        }
        public string ErrorCode { get; }
    }
}