﻿using System;

namespace OverhaulMe.Core.DomainExceptions
{
    public class FurnitureException : Exception, IDomainException
    {
        public FurnitureException()
        {

        }

        public FurnitureException(string errorCode) : this(errorCode, null, null, null)
        {

        }

        public FurnitureException(string message, params object[] args) : this(null, null, message, args)
        {

        }

        protected FurnitureException(string errorCode, string message, params object[] args)
           : this(errorCode, null, message, args)
        {

        }


        public FurnitureException(Exception innerException, string message, params object[] args)
            : this(string.Empty, innerException, message, args)
        {

        }

        public FurnitureException(string errorCode, Exception innerException, string message, params object[] args)
            : base(string.Format(message, args), innerException)
        {
            ErrorCode = errorCode;
        }
        public string ErrorCode { get; }
    }
}