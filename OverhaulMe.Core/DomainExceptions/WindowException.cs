﻿using System;

namespace OverhaulMe.Core.DomainExceptions
{
    public class WindowException : Exception, IDomainException
    {
        public WindowException()
        {

        }

        public WindowException(string errorCode) : this(errorCode, null, null, null)
        {

        }

        public WindowException(string message, params object[] args) : this(null, null, message, args)
        {

        }

        protected WindowException(string errorCode, string message, params object[] args)
           : this(errorCode, null, message, args)
        {

        }


        public WindowException(Exception innerException, string message, params object[] args)
            : this(string.Empty, innerException, message, args)
        {

        }

        public WindowException(string errorCode, Exception innerException, string message, params object[] args)
            : base(string.Format(message, args), innerException)
        {
            ErrorCode = errorCode;
        }
        public string ErrorCode { get; }
    }
}