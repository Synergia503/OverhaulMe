﻿using System;

namespace OverhaulMe.Core.DomainExceptions
{
    public class DuctingException : Exception, IDomainException
    {
        public DuctingException()
        {

        }

        public DuctingException(string errorCode) : this(errorCode, null, null, null)
        {

        }

        public DuctingException(string message, params object[] args) : this(null, null, message, args)
        {

        }

        protected DuctingException(string errorCode, string message, params object[] args)
           : this(errorCode, null, message, args)
        {

        }


        public DuctingException(Exception innerException, string message, params object[] args)
            : this(string.Empty, innerException, message, args)
        {

        }

        public DuctingException(string errorCode, Exception innerException, string message, params object[] args)
            : base(string.Format(message, args), innerException)
        {
            ErrorCode = errorCode;
        }
        public string ErrorCode { get; }
    }
}