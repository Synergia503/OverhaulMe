﻿using OverhaulMe.Core.DomainExceptions;
using System;

namespace OverhaulMe.Core.Domain
{
    public class PaintCan
    {
        protected PaintCan(Room room, string colour, string brandName, float boughtVolume, float remainingVolume, decimal price, DateTime purchasedAt, DateTime? paintedAt)
        {
            RoomId = room.Id;
            SetColour(colour);
            SetBrandName(brandName);
            SetPlace(room.Name);
            SetBoughtVolume(boughtVolume);
            SetRemainingVolume(remainingVolume);
            SetPrice(price);
            PurchasedAt = purchasedAt;
            SetPaintingDate(paintedAt);
        }

        public static PaintCan Create(Room room, string colour, string brandName, float boughtVolume, float remainingVolume, decimal price, DateTime purchasedAt, DateTime? paintedAt)
            => new PaintCan(room, colour, brandName, boughtVolume, remainingVolume, price, purchasedAt, paintedAt);

        public void SetColour(string colour)
        {
            if (string.IsNullOrWhiteSpace(colour))
            {
                throw new PaintCanException(ErrorCodes.EmptyPaintColour, "Colour of paint must not be empty.");
            }

            Colour = colour;
        }

        public void SetBrandName(string brandName)
        {
            if (string.IsNullOrWhiteSpace(brandName))
            {
                throw new PaintCanException(ErrorCodes.EmptyPaintBrandName, "Brand name of paint must not be empty.");
            }

            BrandName = brandName;
        }

        public void SetPlace(string place)
        {
            if (string.IsNullOrWhiteSpace(place))
            {
                throw new PaintCanException(ErrorCodes.EmptyPaintPlace, "Place of paint must not be empty.");
            }

            Place = place;
        }

        public void SetBoughtVolume(float boughtVolume)
        {
            if (boughtVolume <= 0)
            {
                throw new PaintCanException(ErrorCodes.PaintVolumeEqualOrBelowZero, "Bought paint volume must not be below or equal zero.");
            }

            BoughtVolume = boughtVolume;
        }

        public void SetRemainingVolume(float remainingVolume)
        {
            if (remainingVolume < 0)
            {
                throw new PaintCanException(ErrorCodes.PaintVolumeEqualOrBelowZero, "Remaining paint volume must not be below or equal zero");
            }

            RemainingVolume = remainingVolume;
        }

        public void SetPrice(decimal price)
        {
            if (price <= 0)
            {
                throw new PaintCanException(ErrorCodes.PaintPriceEqualOrBelowZero, "Paint has to have price above zero.");
            }

            Price = price;
        }

        public void SetPaintingDate(DateTime? paintedAt)
            => PaintedAt = paintedAt;


        public Guid RoomId { get; protected set; }
        public string Colour { get; protected set; }
        public string BrandName { get; protected set; }
        public string Place { get; protected set; }
        public float BoughtVolume { get; protected set; }
        public float RemainingVolume { get; protected set; }
        public decimal Price { get; protected set; }
        public DateTime PurchasedAt { get; protected set; }
        public DateTime? PaintedAt { get; protected set; }
    }
}