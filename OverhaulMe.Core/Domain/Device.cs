﻿using OverhaulMe.Core.DomainExceptions;
using System;

namespace OverhaulMe.Core.Domain
{
    public class Device
    {
        protected Device(Room room, Guid deviceId, string deviceName, decimal price, bool isNewPurchased, DateTime? purchasedAt = null)
        {
            Id = deviceId;
            RoomId = room.Id;
            SetDeviceName(deviceName);
            SetDevicePrice(price);
            SetDevicePlace(room.Name);
            IsNewPurchased = isNewPurchased;
            PurchasedAt = purchasedAt;
        }

        public static Device Create(Room room, Guid deviceId, string deviceName, decimal price, bool isNewPurchased, DateTime? purchasedAt = null)
           => new Device(room, deviceId, deviceName, price, isNewPurchased, purchasedAt);

        public Guid Id { get; protected set; }
        public Guid RoomId { get; protected set; }
        public string DeviceName { get; protected set; }
        public decimal Price { get; protected set; }
        public string Place { get; protected set; }
        public bool IsNewPurchased { get; protected set; }
        public DateTime? PurchasedAt { get; protected set; }

        public void SetDeviceName(string deviceName)
        {
            if (string.IsNullOrWhiteSpace(deviceName))
            {
                throw new DeviceException(ErrorCodes.EmptyDeviceName, "Device must not have empty name");
            }

            DeviceName = deviceName;
        }

        public void SetDevicePrice(decimal price)
        {
            Price = price <= 0 ? throw new DeviceException(ErrorCodes.DevicePriceEqualOrBelowZero, $"Price of device: '{DeviceName}' must not be equal or below zero.") : price;
        }

        public void SetDevicePlace(string place)
        {
            if (string.IsNullOrWhiteSpace(place))
            {
                throw new DeviceException(ErrorCodes.EmptyPlaceName, "Device must not have empty place");
            }

            Place = place;
        }     
    }
}