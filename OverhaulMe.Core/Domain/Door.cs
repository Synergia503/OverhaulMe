﻿using OverhaulMe.Core.DomainExceptions;
using System;

namespace OverhaulMe.Core.Domain
{
    public class Door
    {
        protected Door(Room room, string secondPlace, float height, float width, float depth, decimal price, string brandName, string kind, bool isNewPurchased, float workHours, DateTime? purchasedAt, DateTime? assmebledAt, DateTime? workStartedAt, DateTime? workEndedAt)
        {
            RoomId = room.Id;
            SetPlaces(room.Name, secondPlace);
            SetHeight(height);
            SetWidth(width);
            SetDepth(depth);
            SetPrice(price);
            SetBrandName(brandName);
            SetKind(kind);
            IsNewPurchased = isNewPurchased;
            AddWorkHours(workHours);
            SetPurchaseDate(purchasedAt);
            SetAssemblyDate(assmebledAt);
            SetStartWorkDate(workStartedAt);
            SetEndWorkDate(workEndedAt);
        }

        public static Door Create(Room room, string secondPlace, float height, float width, float depth, decimal price, string brandName, string kind, bool isNewPurchased, float workHours, DateTime? purchasedAt, DateTime? assmebledAt, DateTime? workStartedAt, DateTime? workEndedAt)
            => new Door(room, secondPlace, height, width, depth, price, brandName, kind, isNewPurchased, workHours, purchasedAt, assmebledAt, workStartedAt, workEndedAt);

        public void SetPlaces(string firstPlace, string secondPlace)
        {
            if (string.IsNullOrWhiteSpace(firstPlace) || string.IsNullOrWhiteSpace(secondPlace))
            {
                throw new DoorException(ErrorCodes.EmptyDoorPlace, "Place of door must not be empty.");
            }

            ConnectedPlaces[0] = firstPlace;
            ConnectedPlaces[1] = secondPlace;
        }

        public void SetHeight(float height)
        {
            if (height <= 0)
            {
                throw new DoorException(ErrorCodes.DoorHeightEqualOrBelowZero, "Height of door must not be below or equal zero.");
            }

            Height = height;
        }


        public void SetWidth(float width)
        {
            if (width <= 0)
            {
                throw new DoorException(ErrorCodes.DoorWidthEqualOrBelowZero, "Width of door must not be below or equal zero.");
            }

            Width = width;
        }

        public void SetDepth(float depth)
        {
            if (depth <= 0)
            {
                throw new DoorException(ErrorCodes.DoorDepthEqualOrBelowZero, "Depth of door must not be below or equal zero.");
            }

            Depth = depth;
        }

        public void SetPrice(decimal price)
        {
            if (price <= 0)
            {
                throw new DoorException(ErrorCodes.DoorPriceEqualOrBelowZero, "Door has to have price above zero.");
            }

            Price = price;
        }

        public void SetBrandName(string brandName)
        {
            if (string.IsNullOrWhiteSpace(brandName))
            {
                throw new DoorException(ErrorCodes.EmptyDoorBrandName, "Brand name of door must not be empty.");
            }

            BrandName = brandName;
        }

        public void SetKind(string kind)
        {
            if (string.IsNullOrWhiteSpace(kind))
            {
                throw new DoorException(ErrorCodes.EmptyDoorKind, "Kind of door must not be empty.");
            }

            Kind = kind;
        }

        public void SetPurchaseDate(DateTime? purchasedAt)
           => PurchasedAt = purchasedAt;

        public void SetAssemblyDate(DateTime? assembledAt)
            => AssembledAt = assembledAt;

        public void SetStartWorkDate(DateTime? startedAt)
           => WorkStartedAt = (WorkEndedAt <= startedAt) ? throw new DoorException(ErrorCodes.WrongDoorStartWorkDate, "Door start work date must not be later than door end work date.") : startedAt;

        public void SetEndWorkDate(DateTime? endedAt)
           => WorkEndedAt = (endedAt <= WorkStartedAt) ? throw new DoorException(ErrorCodes.WrongDoorEndWorkDate, "Door end work date must not be earlier than door start work date.") : endedAt;

        public void AddWorkHours(float hours)
        {
            if (hours <= 0)
            {
                throw new DoorException(ErrorCodes.WorkHoursEqualOrBelowZero, "Work hours at door must not be equal or below zero.");
            }

            WorkHours += hours;
        }

        public Guid RoomId { get; protected set; }
        public string[] ConnectedPlaces { get; protected set; } = new string[2];
        public float Height { get; protected set; }
        public float Width { get; protected set; }
        public float Depth { get; protected set; }
        public decimal Price { get; protected set; }
        public string BrandName { get; protected set; }
        public string Kind { get; protected set; }
        public bool IsNewPurchased { get; protected set; }
        public DateTime? PurchasedAt { get; protected set; }
        public DateTime? AssembledAt { get; protected set; }
        public DateTime? WorkStartedAt { get; protected set; }
        public DateTime? WorkEndedAt { get; protected set; }
        public float WorkHours { get; protected set; }
    }
}