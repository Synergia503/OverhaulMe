﻿using System;
using System.Linq.Expressions;

namespace OverhaulMe.Core.Domain
{
    public interface IFurnitureFactory
    {
        IFurnitureFactory AddPropertyValue(Expression<Func<Furniture, object>> property, object value);
        Furniture Create();
    }
}