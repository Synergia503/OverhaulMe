﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace OverhaulMe.Core.Domain
{
    public class FurnitureFactory : IFurnitureFactory
    {
        private Furniture _furniture;

        public FurnitureFactory(Furniture furniture)
        {
            _furniture = furniture;
        }

        public IFurnitureFactory AddPropertyValue(Expression<Func<Furniture, object>> property, object value)
        {
            PropertyInfo propertyInfo = null;
            if (property.Body is MemberExpression)
            {
                propertyInfo = (property.Body as MemberExpression).Member as PropertyInfo;
            }
            else
            {
                propertyInfo = (((UnaryExpression)property.Body).Operand as MemberExpression).Member as PropertyInfo;
            }

            propertyInfo.SetValue(_furniture, value);

            return this;
        }

        public Furniture Create()
            => _furniture;
    }
}