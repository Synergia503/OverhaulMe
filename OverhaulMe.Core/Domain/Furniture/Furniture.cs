﻿using System;

namespace OverhaulMe.Core.Domain
{
    public class Furniture
    {
        public Furniture()
        {

        }

        public Guid RoomId { get; protected set; }
        public Guid Id { get; protected set; }
        public string Name { get; protected set; }
        public decimal Price { get; protected set; }
        public string Place { get; protected set; }
        public string Purpose { get; protected set; }
        public string BoughtPlace { get; protected set; }
        public bool IsNewPurchased { get; protected set; }
        public DateTime PurchasedAt { get; protected set; }
        public DateTime? MountedAt { get; protected set; }
    }
}