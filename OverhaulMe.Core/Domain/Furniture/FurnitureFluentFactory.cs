﻿namespace OverhaulMe.Core.Domain
{
    public static class FurnitureFluentFactory
    {
        public static IFurnitureFactory Init(Furniture furniture)
            => new FurnitureFactory(furniture);
    }
}