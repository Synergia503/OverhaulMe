﻿using OverhaulMe.Core.DomainExceptions;
using System;

namespace OverhaulMe.Core.Domain
{
    public class Window
    {
        protected Window(Room room, Guid windowId, float height, float width, float depth, decimal price, string brandName, string kind, bool isNewPurchased, DateTime purchasedAt, DateTime? assmebledAt = null)
        {
            RoomId = room.Id;
            Id = windowId;
            SetPlace(room.Name);
            SetHeight(height);
            SetWidth(width);
            SetDepth(depth);
            SetPrice(price);
            SetBrandName(brandName);
            SetKind(kind);
            IsNewPurchased = isNewPurchased;
            PurchasedAt = purchasedAt;
            AssembledAt = assmebledAt;
        }

        public static Window Create(Room room, Guid windowId, float height, float width, float depth, decimal price, string brandName, string kind, bool isNewPurchased, DateTime purchasedAt, DateTime? assmebledAt)
            => new Window(room, windowId, height, width, depth, price, brandName, kind, isNewPurchased, purchasedAt, assmebledAt);

        public void SetPlace(string place)
        {
            if (string.IsNullOrWhiteSpace(place))
            {
                throw new WindowException(ErrorCodes.EmptyWindowPlace, "Place of window must not be empty.");
            }

            Place = place;
        }

        public void SetHeight(float height)
        {
            if (height <= 0)
            {
                throw new WindowException(ErrorCodes.WindowHeightEqualOrBelowZero, "Height of window must not be below or equal zero.");
            }

            Height = height;
        }


        public void SetWidth(float width)
        {
            if (width <= 0)
            {
                throw new WindowException(ErrorCodes.WindowWidthEqualOrBelowZero, "Width of window must not be below or equal zero.");
            }

            Width = width;
        }

        public void SetDepth(float depth)
        {
            if (depth <= 0)
            {
                throw new WindowException(ErrorCodes.WindowDepthEqualOrBelowZero, "Depth of window must not be below or equal zero.");
            }

            Depth = depth;
        }

        public void SetPrice(decimal price)
        {
            if (price <= 0)
            {
                throw new WindowException(ErrorCodes.WindowPriceEqualOrBelowZero, "Window has to have price above zero.");
            }

            Price = price;
        }

        public void SetBrandName(string brandName)
        {
            if (string.IsNullOrWhiteSpace(brandName))
            {
                throw new WindowException(ErrorCodes.EmptyWindowBrandName, "Brand name of window must not be empty.");
            }

            BrandName = brandName;
        }

        public void SetKind(string kind)
        {
            if (string.IsNullOrWhiteSpace(kind))
            {
                throw new WindowException(ErrorCodes.EmptyWindowKind, "Kind of window must not be empty.");
            }

            Kind = kind;
        }

        public void SetAssemblyDate(DateTime? assembledAt)
          => AssembledAt = assembledAt;

        public Guid RoomId { get; protected set; }
        public Guid Id { get; protected set; }
        public string Place { get; protected set; }
        public float Height { get; protected set; }
        public float Width { get; protected set; }
        public float Depth { get; protected set; }
        public decimal Price { get; protected set; }
        public string BrandName { get; protected set; }
        public string Kind { get; protected set; }
        public bool IsNewPurchased { get; protected set; }
        public DateTime PurchasedAt { get; protected set; }
        public DateTime? AssembledAt { get; protected set; }
    }
}