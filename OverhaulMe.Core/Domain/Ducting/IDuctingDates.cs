﻿using System;

namespace OverhaulMe.Core.Domain
{
    public interface IDuctingDates
    {
        IDuctingDates PurchaseDate(DateTime purchasedAt);
        IDuctingDates AssemblyDate(DateTime? assemblyAt);

    }
}