﻿using System;

namespace OverhaulMe.Core.Domain
{
    public interface IDuctingBase
    {
        IDuctingMeasures Create(Guid roomId, string type);
    }
}