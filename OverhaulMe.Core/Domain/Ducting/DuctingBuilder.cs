﻿using System;

namespace OverhaulMe.Core.Domain
{
    public class DuctingBuilder : IDuctingBase, IDuctingMeasures, IDuctingPlaces, IDuctingDates, IDucting
    {
        private Ducting _ducting;

        public IDuctingMeasures Create(Guid roomId, string type)
        {
            _ducting = new Ducting(roomId, type);
            return this;
        }

        public IDuctingMeasures Length(float length)
        {
            _ducting.Length = length;
            return this;
        }

        public IDuctingMeasures Cost(decimal price)
        {
            _ducting.Price = price;
            return this;
        }

        public IDuctingPlaces PlacesConfiguration()
            => this;


        public IDuctingPlaces StayingIn(string place)
        {
            _ducting.Place = place;
            return this;
        }

        public IDuctingPlaces BoughtIn(string boughtPlace)
        {
            _ducting.BoughtPlace = boughtPlace;
            return this;
        }

        public IDuctingDates DatesConfiguration()
            => this;

        public IDuctingDates PurchaseDate(DateTime purchasedAt)
        {
            _ducting.PurchasedAt = purchasedAt;
            return this;
        }

        public IDuctingDates AssemblyDate(DateTime? assemblyAt)
        {
            _ducting.AssembledAt = assemblyAt;
            return this;
        }

        public Ducting Build()
        {
            return _ducting;
        }
    }
}