﻿namespace OverhaulMe.Core.Domain
{
    public interface IDuctingMeasures
    {
        IDuctingMeasures Length(float length);
        IDuctingMeasures Cost(decimal price);
        IDuctingPlaces PlacesConfiguration();
    }
}