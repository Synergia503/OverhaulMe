﻿namespace OverhaulMe.Core.Domain
{
    public interface IDuctingPlaces
    {
        IDuctingPlaces StayingIn(string place);
        IDuctingPlaces BoughtIn(string boughtPlace);
        IDuctingDates DatesConfiguration();
    }
}