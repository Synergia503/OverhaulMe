﻿using OverhaulMe.Core.DomainExceptions;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace OverhaulMe.Core.Domain
{
    public class User
    {
        private static readonly Regex UserNameRegex = new Regex("^(?![_.-])(?!.*[_.-]{2})[a-zA-Z0-9._.-]+(?<![_.-])$");
        private static readonly Regex FullNameRegex = new Regex("^[\\w ]+$");
        private static List<string> _roles = new List<string> { "user", "admin" };

        protected User()
        {

        }

        public User(Guid userId, string email, string userName, string fullName, string password, string salt, string role)
        {
            Id = userId;
            SetEmail(email);
            SetUserName(userName);
            SetFullName(fullName);
            SetPassword(password);
            SetRole(role);
            Salt = salt;
            CreatedAt = DateTime.UtcNow;
        }

        public Guid Id { get; protected set; }
        public string Email { get; protected set; }
        public string Username { get; protected set; }
        public string Password { get; protected set; }
        public string FullName { get; protected set; }
        public string Salt { get; protected set; }
        public string Role { get; protected set; }
        public DateTime CreatedAt { get; protected set; }
        public DateTime UpdatedAt { get; protected set; }

        public void SetFullName(string fullName)
        {
            if (string.IsNullOrWhiteSpace(fullName))
            {
                throw new UserException(ErrorCodes.InvalidFullName, "Full Name can not be empty.");
            }

            if (!FullNameRegex.IsMatch(fullName))
            {
                throw new UserException(ErrorCodes.InvalidFullName, "Full Name is invalid.");
            }

            FullName = fullName;
            UpdatedAt = DateTime.UtcNow;
        }

        public void SetUserName(string username)
        {
            if (!UserNameRegex.IsMatch(username))
            {
                throw new UserException(ErrorCodes.InvalidUserName, "Username is invalid.");
            }

            if (string.IsNullOrWhiteSpace(username))
            {
                throw new UserException(ErrorCodes.InvalidUserName, "Username can not be empty.");
            }

            Username = username;
            UpdatedAt = DateTime.UtcNow;
        }

        public void SetPassword(string password)
        {
            if (string.IsNullOrWhiteSpace(password))
            {
                throw new UserException(ErrorCodes.InvalidPassword, "Password can not be empty.");
            }

            if (password.Length < 4)
            {
                throw new UserException(ErrorCodes.InvalidPassword, "Password must contain at least 4 characters.");
            }

            if (password.Length > 100)
            {
                throw new UserException(ErrorCodes.InvalidPassword, "Password can not contain more than 100 characters.");
            }

            Password = password;
            UpdatedAt = DateTime.UtcNow;
        }

        public void SetEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                throw new UserException(ErrorCodes.InvalidEmail, "Email can not be empty.");
            }

            Email = email.ToLowerInvariant();
            UpdatedAt = DateTime.UtcNow;
        }

        public void SetRole(string role)
        {
            if (string.IsNullOrWhiteSpace(role))
            {
                throw new UserException(ErrorCodes.InvalidRole, "Role can not be empty.");
            }

            role = role.ToLowerInvariant();
            if (!_roles.Contains(role))
            {
                throw new UserException(ErrorCodes.InvalidRole, $"User cannot have a role: '{role}'.");
            }

            Role = role;
            UpdatedAt = DateTime.UtcNow;
        }
    }
}