﻿using OverhaulMe.Core.DomainExceptions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OverhaulMe.Core.Domain
{
    public class Room
    {
        private ISet<Device> _devices = new HashSet<Device>();
        private ISet<Ducting> _ductings = new HashSet<Ducting>();
        private ISet<Furniture> _furnitures = new HashSet<Furniture>();
        private IList<PaintCan> _paintCans = new List<PaintCan>();
        private ISet<Window> _windows = new HashSet<Window>();
        private ISet<Door> _doors = new HashSet<Door>();

        protected Room(Guid userId, Guid id, string name, float floorSize, float wallSize, float ceilingSize)
        {
            UserId = userId;
            Id = id;
            SetRoomName(name);
            SetFloorSize(floorSize);
            SetWallSize(wallSize);
            SetCeilingSize(ceilingSize);
        }

        protected Room()
        {

        }

        public static Room Create(Guid userId, Guid id, string name, float floorSize, float wallSize, float ceilingSize)
            => new Room(userId, id, name, floorSize, wallSize, ceilingSize);

        public void SetRoomName(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new RoomException(ErrorCodes.EmptyRoomName, "Room must not have empty name");
            }

            Name = name;
        }

        public void SetFloorSize(float floorSize)
            => FloorSize = floorSize <= 0 ?
              throw new RoomException(ErrorCodes.FloorSizeEqualOrBelowZero, "Floor must not have size below or equal zero") : floorSize;

        public void SetWallSize(float wallSize)
            => WallSize = wallSize <= 0 ?
              throw new RoomException(ErrorCodes.WallSizeEqualOrBelowZero, "Wall must not have size below or equal zero") : wallSize;

        public void SetCeilingSize(float ceilingSize)
           => CeilingSize = ceilingSize <= 0 ?
             throw new RoomException(ErrorCodes.CeilingSizeEqualOrBelowZero, "Ceiling must not have size below or equal zero") : ceilingSize;

        public Guid Id { get; protected set; }
        public string Name { get; protected set; }
        public float FloorSize { get; protected set; }
        public float WallSize { get; protected set; }
        public float CeilingSize { get; protected set; }
        public Guid UserId { get; protected set; }

        public IEnumerable<Ducting> Ductings => _ductings;
        public IEnumerable<Device> Devices => _devices;
        public IEnumerable<Furniture> Furnitures => _furnitures;
        public IList<PaintCan> PaintCans => _paintCans;
        public IEnumerable<Door> Doors => _doors;
        public IEnumerable<Window> Windows => _windows;

        public void AddDevice(Device device)
        {
            _devices.Add(device);
        }

        public void RemoveDevice(Device device)
        {
            _devices.Remove(device);
        }

        public void AddDucting(Ducting ducting)
        {
            _ductings.Add(ducting);
        }

        public void RemoveDucting(Ducting ducting)
        {
            _ductings.Remove(ducting);
        }

        public void AddFurniture(Furniture furniture)
        {
            _furnitures.Add(furniture);
        }

        public void RemoveFurniture(Furniture furniture)
        {
            _furnitures.Remove(furniture);
        }

        public void AddWindow(Window window)
        {
            _windows.Add(window);
        }

        public void RemoveWindow(Window window)
        {
            _windows.Remove(window);
        }

        public void AddDoor(Door door)
        {
            _doors.Add(door);
        }

        public void RemoveDoor(Door door)
        {
            _doors.Remove(door);
        }

        public void AddPaintCan(PaintCan paintCan)
        {
            _paintCans.Add(paintCan);
        }

        public void RemovePaintCans(int numberOfPaintCansToRemove, string brandName)
        {
            for (int i = 0; i < numberOfPaintCansToRemove; i++)
            {
                _paintCans.Remove(_paintCans.FirstOrDefault());
            }
        }
    }
}