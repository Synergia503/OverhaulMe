﻿using OverhaulMe.Core.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OverhaulMe.Core.Repositories
{
    public interface IRoomRepository : IRepository
    {
        Task<IEnumerable<Room>> GetRoomsForUser(Guid userId);
        Task<Room> GetSingleRoomForUser(Guid userId, Guid roomId);
        Task<Room> GeByIdAsync(Guid roomId);
        Task<IEnumerable<Room>> BrowseAsync();
        Task AddAsync(Room room);
        Task UpdateAsync(Room room);
        Task DeleteAsync(Room room);
    }
}